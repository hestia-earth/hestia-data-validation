import os

from hestia_earth.validation.terms import TERMS_QUERY

fixtures_path = os.path.abspath('tests/fixtures')

FUEL_TERM_IDS = [
    'petrol',
    'diesel'
]
CROP_RESIDUE_TERM_IDS = [
    'aboveGroundCropResidueRemoved', 'aboveGroundCropResidueIncorporated', 'aboveGroundCropResidueTotal',
    'aboveGroundCropResidueLeftOnField', 'aboveGroundCropResidueBurnt',
    'belowGroundCropResidue'
]
FORAGE_TERM_IDS = [
    'wheatFreshForage'
]
RICE_TERM_IDS = [
    'riceMeal', 'riceGrainInHuskUpland'
]
MODEL_TERM_IDS = [
    'ipcc2007'
]


def fake_get_terms(query):
    return {
        TERMS_QUERY.CROP_RESIDUE: CROP_RESIDUE_TERM_IDS,
        TERMS_QUERY.FORAGE: FORAGE_TERM_IDS,
        TERMS_QUERY.FUEL: FUEL_TERM_IDS,
        TERMS_QUERY.MODEL: MODEL_TERM_IDS,
        TERMS_QUERY.RICE: RICE_TERM_IDS
    }.get(query, [])
