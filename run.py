# usage:
# 1. Install python-dotenv
# 2. Set your env variables in a `.env` file
# 3. Put your test nodes into the `samples` folder
# 3. Run `python run.py samples/nodes.jsonld`
from dotenv import load_dotenv
load_dotenv()


import os
import sys
import json
import numpy as np
from hestia_earth.models.preload_requests import enable_preload
from hestia_earth.validation import validate


# fix error "Object of type int64 is not JSON serializable"
class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        if isinstance(obj, np.floating):
            return float(obj)
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return super(NpEncoder, self).default(obj)


def _enable_mock_models():
    filename = 'models-search-results.json'
    tmp_filepath = os.path.join('.', filename)
    enable_preload(tmp_filepath, overwrite_existing=False, use_glossary=True)


def main(args):
    filepath = args[0]
    with open(filepath) as f:
        data = json.load(f)

    _enable_mock_models()

    print(f"processing {filepath}")
    data = validate(data if isinstance(data, list) else data.get('nodes', [data]))

    with open(f"{filepath.split('.')[0]}-validated.json", 'w') as f:
        f.write(json.dumps(data, indent=2, ensure_ascii=False, cls=NpEncoder))


if __name__ == "__main__":
    main(sys.argv[1:])
