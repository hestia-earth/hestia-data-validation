# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.32.22](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.32.21...v0.32.22) (2025-03-04)


### Features

* **source:** remove validation on `defaultSource` ([0a52a1c](https://gitlab.com/hestia-earth/hestia-data-validation/commit/0a52a1cc82272c02c9ae2f83538efef94e57abd7))

### [0.32.21](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.32.20...v0.32.21) (2025-02-08)


### Features

* add 1% rouding error when validating value between min and max ([128804d](https://gitlab.com/hestia-earth/hestia-data-validation/commit/128804d2e05d73f32100399263b84eb4c61639a1))

### [0.32.20](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.32.19...v0.32.20) (2025-02-04)


### Bug Fixes

* **practice:** allow `0` values when validating rice terms ([152ac91](https://gitlab.com/hestia-earth/hestia-data-validation/commit/152ac91ef723b287faaaf0c2f451bfbd7307e64b))

### [0.32.19](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.32.18...v0.32.19) (2025-01-23)


### Features

* **cycle:** validate practices `valueType` ([fc78517](https://gitlab.com/hestia-earth/hestia-data-validation/commit/fc78517abd66f651a385cdcb2fadab8e62e6d146))
* **shared:** add validator for blank nodes `valueType` ([193d29d](https://gitlab.com/hestia-earth/hestia-data-validation/commit/193d29d0c487b2db34998330b297cbf202571d2b))
* **site:** validate measurements and management `valueType` ([e474ce1](https://gitlab.com/hestia-earth/hestia-data-validation/commit/e474ce16e62d250af2768749d1bfa6bf924dcbce))


### Bug Fixes

* **measurement:** fix parsing of measurement value as a list ([ba73f45](https://gitlab.com/hestia-earth/hestia-data-validation/commit/ba73f451318794f12376a9e19b5f088a5c1df3bd))

### [0.32.18](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.32.17...v0.32.18) (2025-01-21)


### Features

* **gee:** cache data in batches ([da42eb1](https://gitlab.com/hestia-earth/hestia-data-validation/commit/da42eb1b75e5d2fb5bcd79520d627819a86b447a))

### [0.32.17](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.32.16...v0.32.17) (2025-01-08)


### Bug Fixes

* **site:** handle no cached data on the Site ([77f5f5a](https://gitlab.com/hestia-earth/hestia-data-validation/commit/77f5f5add5bace34b3251cfa04bb4c5910f82090))

### [0.32.16](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.32.15...v0.32.16) (2024-12-20)


### Features

* **cycle:** validate `saplingsDepreciatedAmountPerCycle` inputs value ([74bfd1b](https://gitlab.com/hestia-earth/hestia-data-validation/commit/74bfd1bda395132ebb7eb834a64826cf3584c205))
* **cycle:** validate range error for `value` `min` and `max` on `inputs` ([d43d96a](https://gitlab.com/hestia-earth/hestia-data-validation/commit/d43d96a2fe9fc63bab52afd6f20a8caaae320c99))
* **product:** validate excreta using `system` if provided ([37d5c25](https://gitlab.com/hestia-earth/hestia-data-validation/commit/37d5c259c714c44fcda9d847cb5d751696f7aef2))

### [0.32.15](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.32.14...v0.32.15) (2024-12-11)


### Bug Fixes

* **terms:** fix get terms from cache still running search ([ae401ea](https://gitlab.com/hestia-earth/hestia-data-validation/commit/ae401ea82db7b3acfda6ae719fbc2d8bbf1afd86))

### [0.32.14](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.32.13...v0.32.14) (2024-12-09)

### [0.32.13](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.32.12...v0.32.13) (2024-12-06)

### [0.32.12](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.32.11...v0.32.12) (2024-12-06)


### Features

* **animal:** add validation should specify `pregnancyRateTotal` ([f25b8fa](https://gitlab.com/hestia-earth/hestia-data-validation/commit/f25b8fabacd0a919a53818f59928ba3406ade75e))
* **cycle:** use `endDate` and `startDate` to validate maximum cycle duration ([e62f3e2](https://gitlab.com/hestia-earth/hestia-data-validation/commit/e62f3e23a2127a61e97d50b21848255929ae9d60))


### Bug Fixes

* **cycle:** fix error pregnancy rate check ([83c569d](https://gitlab.com/hestia-earth/hestia-data-validation/commit/83c569d0880c4b0a4b4e8f80fbc37a1370455b69))

### [0.32.11](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.32.10...v0.32.11) (2024-11-27)


### Features

* **indicator:** validate `inoisingCompounds` must have a single waste input ([d28d4bd](https://gitlab.com/hestia-earth/hestia-data-validation/commit/d28d4bd54ad8a23c199af4750c16cba3048154a4))
* **practice:** handle animalProduct in system validation ([c71f4a1](https://gitlab.com/hestia-earth/hestia-data-validation/commit/c71f4a1f78f34ecb72ff57d6b37a70e213dd3542))
* **site:** toggle validate linked cycles via ia with env var ([267f9ed](https://gitlab.com/hestia-earth/hestia-data-validation/commit/267f9ed07a53b9973a2684a74128f000d6e2966c))

### [0.32.10](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.32.9...v0.32.10) (2024-11-25)


### Bug Fixes

* **source:** allow uploading only Source and Actor ([d67473d](https://gitlab.com/hestia-earth/hestia-data-validation/commit/d67473def07263bbe28ccab48d0e60f29de2fabe))

### [0.32.9](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.32.8...v0.32.9) (2024-11-19)


### Features

* **practice:** validate requires `productivePhasePermanentCrops` ([2e44ed2](https://gitlab.com/hestia-earth/hestia-data-validation/commit/2e44ed26a548d496b96ec08a006d970666413fc2))


### Bug Fixes

* handle no blank node term check duplicated units ([a0f08e2](https://gitlab.com/hestia-earth/hestia-data-validation/commit/a0f08e2a044ca1bc492864871ccd0eed77178f71)), closes [#372](https://gitlab.com/hestia-earth/hestia-data-validation/issues/372)

### [0.32.8](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.32.7...v0.32.8) (2024-10-30)


### Bug Fixes

* **completeness:** fix fuel use require any material with positive value ([7b8f254](https://gitlab.com/hestia-earth/hestia-data-validation/commit/7b8f254579a3a7ac48900267959f03b0ca6063ad))

### [0.32.7](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.32.6...v0.32.7) (2024-10-29)


### Bug Fixes

* **cycle:** handle no `cycleDuration` for rice ([d29aecd](https://gitlab.com/hestia-earth/hestia-data-validation/commit/d29aecd5dbfd1469f9fb503a11778e0d65716590))

### [0.32.6](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.32.5...v0.32.6) (2024-10-28)


### Features

* **cycle:** validate min `cycleDuration` for flooded rice ([6fe08f8](https://gitlab.com/hestia-earth/hestia-data-validation/commit/6fe08f87159f1b9525650c6080374ff08bff5c77))
* **property:** validate `value` is between `min` and `max` ([dbc87b4](https://gitlab.com/hestia-earth/hestia-data-validation/commit/dbc87b4b4be0070bda32d8203a734bb0b31b20a5))
* **source:** prevent adding public source without link to `defaultSource` ([9dc7105](https://gitlab.com/hestia-earth/hestia-data-validation/commit/9dc7105f6eaecd7f37bd3ed17533a84816f569f5))
* validate `methodModel=otherModel` should add description ([79e95c0](https://gitlab.com/hestia-earth/hestia-data-validation/commit/79e95c0454cf14087984f20c6118308c50337339))

### [0.32.5](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.32.4...v0.32.5) (2024-10-14)


### Bug Fixes

* **practice:** remove validation 0 tillages requires practice ([45c4bcc](https://gitlab.com/hestia-earth/hestia-data-validation/commit/45c4bccfa6a6375ffd56c86a78730ae41cc2eba4))

### [0.32.4](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.32.3...v0.32.4) (2024-10-11)


### Bug Fixes

* **practice:** validate rice `waterRegime` on primary product only ([db13361](https://gitlab.com/hestia-earth/hestia-data-validation/commit/db133616ca2f15a36fe86ed3683807105134389d))

### [0.32.3](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.32.2...v0.32.3) (2024-09-17)


### Bug Fixes

* **practice:** skip non-water regime practices ([c23ec09](https://gitlab.com/hestia-earth/hestia-data-validation/commit/c23ec09abf90b6ebd24b2b218b1862fc5a2bf7a5))

### [0.32.2](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.32.1...v0.32.2) (2024-09-17)


### Bug Fixes

* **practice:** handle `allowedRiceTermIds` not set ([d1cfa89](https://gitlab.com/hestia-earth/hestia-data-validation/commit/d1cfa89805189f273876839138a22aa5ca388cb0))

### [0.32.1](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.32.0...v0.32.1) (2024-09-17)


### Features

* **cycle:** validate `croppingDuration` value for `riceGrainInHuskFlooded` ([30a1999](https://gitlab.com/hestia-earth/hestia-data-validation/commit/30a1999b88d940640c25e67474837c04cc1f8456))
* **impact_assessment:** validate must not be empty ([178621c](https://gitlab.com/hestia-earth/hestia-data-validation/commit/178621c77925e5416949b47dc009d605c8c6eb08))

## [0.32.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.31.3...v0.32.0) (2024-08-30)


### ⚠ BREAKING CHANGES

* **requirements:** Supported schema version is `30`.

### Features

* **requirements:** require schema `30` ([090c8a6](https://gitlab.com/hestia-earth/hestia-data-validation/commit/090c8a66b9cadc5924ab98e4e1d42dd99f759283))

### [0.31.3](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.31.2...v0.31.3) (2024-08-26)


### Bug Fixes

* **cycle:** handle no `cycleDuration` or `siteDuration` ([51c1323](https://gitlab.com/hestia-earth/hestia-data-validation/commit/51c132318c7c660c14a0925e2d2eea680b012773))

### [0.31.2](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.31.1...v0.31.2) (2024-08-23)


### Features

* **cycle:** validate `siteDuration` for crop cycles ([451f0cf](https://gitlab.com/hestia-earth/hestia-data-validation/commit/451f0cfdc655cae84f7ac6e05513fd8014eb34c4))
* **practice:** use `longFallowDuration` ([fa2f56a](https://gitlab.com/hestia-earth/hestia-data-validation/commit/fa2f56a753e040cb65a03e5809680cafc5a7e303))

### [0.31.1](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.31.0...v0.31.1) (2024-08-06)


### Features

* **cycle:** validate maximum `cycleDuration` ([0d3aabe](https://gitlab.com/hestia-earth/hestia-data-validation/commit/0d3aabe1545cecea5c36f16990a4810f27f3e3c7))
* **cycle:** validate substrate sytem requires substrate input ([0334c32](https://gitlab.com/hestia-earth/hestia-data-validation/commit/0334c32896a7c57bc4f2bdd0061427ac60244474))
* **site:** validate sum 100% on management blank nodes ([8899129](https://gitlab.com/hestia-earth/hestia-data-validation/commit/8899129bfd1c26c26557e8921e272877448e72e0))
* **site:** validate water salinity ([a64d8d5](https://gitlab.com/hestia-earth/hestia-data-validation/commit/a64d8d5e6d960733e183dc5335329207dbf1b58e))

## [0.31.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.30.0...v0.31.0) (2024-07-23)


### ⚠ BREAKING CHANGES

* **requirements:** Supported schema version is now `29.0.0`.

### Features

* **requirements:** use schema `29.0.0` ([3c443de](https://gitlab.com/hestia-earth/hestia-data-validation/commit/3c443de225c866bc8864787911563b8ae94f78ac))

## [0.30.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.29.8...v0.30.0) (2024-06-25)


### ⚠ BREAKING CHANGES

* **requirements:** Schema version required is `28`.

### Features

* **requirements:** use schema `28` ([74d65e1](https://gitlab.com/hestia-earth/hestia-data-validation/commit/74d65e1c5970e0bd99286d159dec11790bbb82fe))
* **site:** validate pond has required measurements ([4ca8683](https://gitlab.com/hestia-earth/hestia-data-validation/commit/4ca8683db90607939087448ec5353a5325a87735))


### Bug Fixes

* **site:** remove `Management` node validations ([5f7e38f](https://gitlab.com/hestia-earth/hestia-data-validation/commit/5f7e38f55cce39c152fc098f4422407363364993))

### [0.29.8](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.29.7...v0.29.8) (2024-05-30)


### Features

* **animal:** validate input duplicated with Cycle feed input ([eea642b](https://gitlab.com/hestia-earth/hestia-data-validation/commit/eea642b38f0d730bcfd61676a8dab61aa569b236))
* **management:** valid fallow terms duration between intervals ([e9217a7](https://gitlab.com/hestia-earth/hestia-data-validation/commit/e9217a7261b3ed4418e4129c92d23b08e7d088a1))


### Bug Fixes

* handle values as array when validating list sum ([2e6cdc4](https://gitlab.com/hestia-earth/hestia-data-validation/commit/2e6cdc402a80c9abe33f912ad3d5f7f5b818b05d)), closes [#336](https://gitlab.com/hestia-earth/hestia-data-validation/issues/336)
* **practice:** ignore `%` units check default value ([5fe5782](https://gitlab.com/hestia-earth/hestia-data-validation/commit/5fe5782db94bcbf413e0128d29367f5aae59e3dc))

### [0.29.7](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.29.6...v0.29.7) (2024-05-09)


### Features

* **cycle:** validate allowed `animalProduct` from `liveAnimal` ([3f4ded0](https://gitlab.com/hestia-earth/hestia-data-validation/commit/3f4ded0f392b70b99655f5cbfbf17e8abe108646))
* **impact-assessment:** fallback validate product in Cycle unique ([68fdd19](https://gitlab.com/hestia-earth/hestia-data-validation/commit/68fdd191e888c4ceff64f990168c709a030dda6c))

### [0.29.6](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.29.5...v0.29.6) (2024-05-02)


### Features

* **practice:** validate tillage values ([c5ba356](https://gitlab.com/hestia-earth/hestia-data-validation/commit/c5ba35616aab5c19e2836406721ec2bd3ba5fdfd))
* **site:** validate overlapping cycles on certain `siteType` ([81f6439](https://gitlab.com/hestia-earth/hestia-data-validation/commit/81f6439d4771237dc8497a95cca37f221c0bcf32))

### [0.29.5](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.29.4...v0.29.5) (2024-04-29)


### Features

* **site:** require value for measurment with units `% area` ([df38dce](https://gitlab.com/hestia-earth/hestia-data-validation/commit/df38dce0eb4640eac3dada6c94bd3810ee9399de))
* **site:** validate cycles linked by ia on the same site ([49cea32](https://gitlab.com/hestia-earth/hestia-data-validation/commit/49cea32ca909d1e19e1455c4050e0268d3871cd7))
* **site:** validate multiple Cycle overlapping in dates ([6fc595f](https://gitlab.com/hestia-earth/hestia-data-validation/commit/6fc595f5b85f32b1121b4188a557aa4a9094a2be))


### Bug Fixes

* **shared:** fix `dataPath` on required `value` field ([1bfe28a](https://gitlab.com/hestia-earth/hestia-data-validation/commit/1bfe28a1719c6e78f16dfc98a7303afab3ec8fc7))

### [0.29.4](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.29.3...v0.29.4) (2024-03-27)


### Features

* **cycle:** skip validating linked IA on aggregated Cycle ([137f03e](https://gitlab.com/hestia-earth/hestia-data-validation/commit/137f03e003579b8bff6eaa4c848e850197206ba8))


### Bug Fixes

* **shared:** add missing param `missingProperty` ([5a392e9](https://gitlab.com/hestia-earth/hestia-data-validation/commit/5a392e99e36a2ed6955abefeaab8cd4162c505fd))

### [0.29.3](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.29.2...v0.29.3) (2024-03-27)


### Bug Fixes

* **cycle:** handle `generateImpactAssessment` stored as a string ([e73cd4e](https://gitlab.com/hestia-earth/hestia-data-validation/commit/e73cd4ef03bea2ac6222a1ef7171bc80bd4b57c8))

### [0.29.2](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.29.1...v0.29.2) (2024-03-26)


### Features

* **cycle:** validate every product has a linked IA ([466b9ad](https://gitlab.com/hestia-earth/hestia-data-validation/commit/466b9ad4e0e42794dc683c87ca93fcff14c03e8e))
* **measurement:** use `depthSensitive` to flag error on missing depths ([12eba2c](https://gitlab.com/hestia-earth/hestia-data-validation/commit/12eba2cb80319c8ce6472386a52cd568a2cd6ffb))

### [0.29.1](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.29.0...v0.29.1) (2024-03-15)


### Features

* **cycle:** validate list of practices must have a `value` ([13d40eb](https://gitlab.com/hestia-earth/hestia-data-validation/commit/13d40eba464331331498ecd762e24f5ffb23a3ee))

## [0.29.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.28.10...v0.29.0) (2024-03-05)


### ⚠ BREAKING CHANGES

* **completenesss:** Min schema version is `27.0.0`.

### Features

* **completenesss:** use `freshForage` instead of `grazedForage` ([571582a](https://gitlab.com/hestia-earth/hestia-data-validation/commit/571582ab308925d784f4737cc7cdd25b1bb97b41))
* remove specific validation for aggregated data ([0d59773](https://gitlab.com/hestia-earth/hestia-data-validation/commit/0d5977396d39e72b2750578d017d6d41ae89fc37))

### [0.28.10](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.28.9...v0.28.10) (2024-03-01)


### Features

* **cycle:** validate `1 ha` forbidden on `food retailer` siteType ([3abb095](https://gitlab.com/hestia-earth/hestia-data-validation/commit/3abb09501d9c34a225e78d83705c4873487e6a8f))
* **impact-assessment:** remove skip validating impacts on aggregated ([8519793](https://gitlab.com/hestia-earth/hestia-data-validation/commit/8519793dfb6f05604cf3b8de5b79801b11a08de9))


### Bug Fixes

* handle `otherSites` without `siteType` ([e42b48d](https://gitlab.com/hestia-earth/hestia-data-validation/commit/e42b48d27629d49fc40cab1d35aacf06d51dde68))

### [0.28.9](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.28.8...v0.28.9) (2024-02-21)


### Features

* **impact-assessment:** disable validation of aggregated `impacts` using models ([1778f9b](https://gitlab.com/hestia-earth/hestia-data-validation/commit/1778f9b380bb2681e64048e31c6f97024b23609e))
* **shared:** handle variable tolerance for models result ([62aea4c](https://gitlab.com/hestia-earth/hestia-data-validation/commit/62aea4c9c574f98d901f2ad818c97566347cbc52))

### [0.28.8](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.28.7...v0.28.8) (2024-02-19)


### Features

* **models:** use env var `VALIDATE_MODELS_SKIP` to skip validation models ([39455f4](https://gitlab.com/hestia-earth/hestia-data-validation/commit/39455f44e1efe3e4c12401b835baa02f07b1708d))

### [0.28.7](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.28.6...v0.28.7) (2024-02-16)


### Bug Fixes

* **indicator:** fix validation of land transformation ([5b67ee6](https://gitlab.com/hestia-earth/hestia-data-validation/commit/5b67ee63950015d95f98ed9368f3a060849cb19f))

### [0.28.6](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.28.5...v0.28.6) (2024-02-15)


### Bug Fixes

* handle floating errors on min/max check ([c28b4c2](https://gitlab.com/hestia-earth/hestia-data-validation/commit/c28b4c2ed40d03a13860fb6e5112cf1771763f5b))
* **input:** do not require `isAnimalFeed` on `liveAnimal` ([e7db8a8](https://gitlab.com/hestia-earth/hestia-data-validation/commit/e7db8a8846f50d2099e59635f1c2608b23c554b9)), closes [#301](https://gitlab.com/hestia-earth/hestia-data-validation/issues/301)

### [0.28.5](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.28.4...v0.28.5) (2024-02-13)


### Bug Fixes

* **cycle:** allow below crop residue is `0` ([f1437e0](https://gitlab.com/hestia-earth/hestia-data-validation/commit/f1437e0e08594b8f348abef77b8b7bd080bc959d))

### [0.28.4](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.28.3...v0.28.4) (2024-02-08)


### Features

* **validation:** add env var to validate aggregated data fully ([7104731](https://gitlab.com/hestia-earth/hestia-data-validation/commit/71047314c1cf7a63275369bdd6ec305a6affde87))

### [0.28.3](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.28.2...v0.28.3) (2024-02-06)


### Features

* **shared:** allow 1% rounding error on min/max error ([182d9ba](https://gitlab.com/hestia-earth/hestia-data-validation/commit/182d9ba32baaed29fe1682e592672a92d6803c32))


### Bug Fixes

* **practice:** fix check long fallow ratio below 5 years not 5 days ([c8da4dd](https://gitlab.com/hestia-earth/hestia-data-validation/commit/c8da4dd5879029b162677eaf8bd1b4595c63064d))

### [0.28.2](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.28.1...v0.28.2) (2024-02-02)


### Features

* **completeness:** allow `animals` inputs to validate `grazedForage` ([6b162ba](https://gitlab.com/hestia-earth/hestia-data-validation/commit/6b162baac6ae52723545cfe51a049e081311d4c9))
* **completeness:** update `grazedForage` to check only grazing animals ([d6962e2](https://gitlab.com/hestia-earth/hestia-data-validation/commit/d6962e21669551228feea15d14f478644e967894))
* **completeness:** validate `ingredient` ([04759d4](https://gitlab.com/hestia-earth/hestia-data-validation/commit/04759d41f4b67d78e132e56c0a75efb8739b31fb)), closes [#294](https://gitlab.com/hestia-earth/hestia-data-validation/issues/294) [#295](https://gitlab.com/hestia-earth/hestia-data-validation/issues/295)
* **input:** validate animal inputs for `isAnimalFeed` ([5fe0d95](https://gitlab.com/hestia-earth/hestia-data-validation/commit/5fe0d9598b0a70bf803b4a9a62c57b751f554580))
* **measurement:** validate more measurements ([b08445e](https://gitlab.com/hestia-earth/hestia-data-validation/commit/b08445e7a1e568776a8d3be28805e677c311bf97))


### Bug Fixes

* **input:** require `processedFood` and `waste` to specify `isAnimalFeed` ([e9a1dba](https://gitlab.com/hestia-earth/hestia-data-validation/commit/e9a1dbae948e49ba7ce6065b82a8d23ff8f718ea))

### [0.28.1](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.28.0...v0.28.1) (2024-01-22)


### Bug Fixes

* **distribution:** handle value is `nan` ([391bb4b](https://gitlab.com/hestia-earth/hestia-data-validation/commit/391bb4bea631caa1e4a2b5beace0409c48406f8f)), closes [#289](https://gitlab.com/hestia-earth/hestia-data-validation/issues/289)

## [0.28.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.27.0...v0.28.0) (2024-01-03)


### ⚠ BREAKING CHANGES

* Min schema version required is `26.0.0`.

### Features

* update to schema 26 ([166b931](https://gitlab.com/hestia-earth/hestia-data-validation/commit/166b9316bf573919bf605ca0538b5e2399ee6306))


### Bug Fixes

* **completeness:** use `grazedForage` instead of `animalFeed` ([b18dac8](https://gitlab.com/hestia-earth/hestia-data-validation/commit/b18dac81d42c9854e6c189f114ac2be81223eb8b))
* **cycle:** fix validate unique IA linked to product with different unique fields ([cefba1b](https://gitlab.com/hestia-earth/hestia-data-validation/commit/cefba1bf27dc7ab2c64ef2ab5266d031fb36f153))
* **practice:** validate pastureGrass key is `landCover` ([701a3ef](https://gitlab.com/hestia-earth/hestia-data-validation/commit/701a3ef02c96f4f6e1021a0fe316b82d52ebc854))

## [0.27.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.26.0...v0.27.0) (2023-12-18)


### ⚠ BREAKING CHANGES

* Min schema version required is `25.0.0`.

### Features

* **cycle:** validate stocking density specified on permanent pasture ([c337054](https://gitlab.com/hestia-earth/hestia-data-validation/commit/c337054f749f9b1cfbbb06bb40be64b90a98688e))
* update to schema 25 ([2c13934](https://gitlab.com/hestia-earth/hestia-data-validation/commit/2c139349e6933a4631de8fb78118a41443151edc))

## [0.26.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.25.4...v0.26.0) (2023-11-21)


### ⚠ BREAKING CHANGES

* **package:** Min schema version required is `24.0.0`.

* **package:** update schema to `24.1.0` ([346fc4b](https://gitlab.com/hestia-earth/hestia-data-validation/commit/346fc4bdeb44ca9e22341cd5c7fe741c3e6a67e3))

### [0.25.4](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.25.3...v0.25.4) (2023-11-14)


### Bug Fixes

* **gee:** fix validation on all nodes when caching enabled ([2faeef1](https://gitlab.com/hestia-earth/hestia-data-validation/commit/2faeef1df8290618d29694a1c95c329c17eedb0c))

### [0.25.3](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.25.2...v0.25.3) (2023-11-10)


### Features

* **mocking:** log search result on debug only ([26db537](https://gitlab.com/hestia-earth/hestia-data-validation/commit/26db537f378f4ed32f1d4c2a1c9ad5955439e6ca))

### [0.25.2](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.25.1...v0.25.2) (2023-11-03)


### Features

* **gee:** handle caching years ([5e341b9](https://gitlab.com/hestia-earth/hestia-data-validation/commit/5e341b9ee119a237f4a04edb5b33c665ebccc2f2))

### [0.25.1](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.25.0...v0.25.1) (2023-10-31)


### Bug Fixes

* **bin:** fix encoding issue with numpy ([dafc4f5](https://gitlab.com/hestia-earth/hestia-data-validation/commit/dafc4f5e60ec2c7981672691208a6671f6525a17))

## [0.25.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.24.0...v0.25.0) (2023-10-24)


### ⚠ BREAKING CHANGES

* **site:** Min required version of `hestia_earth.earth_engine` is `0.4.0`.

### Bug Fixes

* **distribution:** handle no `min` value ([7fa7cc8](https://gitlab.com/hestia-earth/hestia-data-validation/commit/7fa7cc8adb2656b9feb0531d5d7ec63af23ae991))


* **site:** update `earth_engine` to `0.4.0` ([0b469f6](https://gitlab.com/hestia-earth/hestia-data-validation/commit/0b469f62ca5bb3177ac7b6e0a7af6d47aa45bb72))

## [0.24.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.23.4...v0.24.0) (2023-10-10)


### ⚠ BREAKING CHANGES

* Validation using models requires version `0.52.0`.

* update models to `0.52.0` and earth engine to `0.3.0` ([a5f0d89](https://gitlab.com/hestia-earth/hestia-data-validation/commit/a5f0d8916ecf5117d14ae2e32ba31d4723f6d290))

### [0.23.4](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.23.3...v0.23.4) (2023-10-09)


### Features

* **management:** validate must contain at least one blank node ([3e4705c](https://gitlab.com/hestia-earth/hestia-data-validation/commit/3e4705c16673408f812cc7ed553ebe8b61dc7729))


### Bug Fixes

* **cycle:** remove `cropResidue` validation on `permanent pasture` ([2cb813e](https://gitlab.com/hestia-earth/hestia-data-validation/commit/2cb813e51401cb6bcea70962e8b3f6b3306b0b6c))
* **management:** validate `termType` only if blank nodes are present ([f50cd5e](https://gitlab.com/hestia-earth/hestia-data-validation/commit/f50cd5e39ec1da7a93f9897724bdf8615304c897))
* **property:** fix incorrect error path ([bf78afe](https://gitlab.com/hestia-earth/hestia-data-validation/commit/bf78afe48772b08b1141f7fe71972e09a17f3b73))

### [0.23.3](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.23.2...v0.23.3) (2023-09-15)


### Features

* use `boundaryArea` instead of calculating `boundary` area ([7b3842c](https://gitlab.com/hestia-earth/hestia-data-validation/commit/7b3842c04d6682ea748481524701aedc50d28b61))

### [0.23.2](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.23.1...v0.23.2) (2023-09-11)


### Bug Fixes

* **aggregated:** set level to `warning` for organic or irrigated nodes ([b103aa6](https://gitlab.com/hestia-earth/hestia-data-validation/commit/b103aa675aa2311b7d703376fd9c6d005151251c))

### [0.23.1](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.23.0...v0.23.1) (2023-09-04)


### Features

* **management:** add validation on `termType` should contain 1 entry ([4593861](https://gitlab.com/hestia-earth/hestia-data-validation/commit/45938615d9fcff955c4894bf17b094ccbff54bf8))
* **property:** validate blank node `termType` allowed ([df7d259](https://gitlab.com/hestia-earth/hestia-data-validation/commit/df7d259ce4d2587d2e39823a3411bce379ba4a5d))
* **requirements:** set min distribution version to `0.0.14` ([0b84e4a](https://gitlab.com/hestia-earth/hestia-data-validation/commit/0b84e4af6236df96f00caf86816ff29e784cd291))

## [0.23.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.22.7...v0.23.0) (2023-08-02)


### ⚠ BREAKING CHANGES

* **requirements:** Requires schema version `22`.

### Features

* handle aggregated `Cycle` and `ImpactAssessment` ([ecac4bd](https://gitlab.com/hestia-earth/hestia-data-validation/commit/ecac4bd887a01054d0f987dcaa4c3e67e9b4bcfb))
* **product:** restrict yield validation to cropland only ([34e194c](https://gitlab.com/hestia-earth/hestia-data-validation/commit/34e194c5fb99acffa728275e8a11d75d575004d1)), closes [#249](https://gitlab.com/hestia-earth/hestia-data-validation/issues/249)
* **requirements:** use schema version `22` ([6acde13](https://gitlab.com/hestia-earth/hestia-data-validation/commit/6acde135908aae13b98cb8d1e420cf85252b0082))

### [0.22.7](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.22.6...v0.22.7) (2023-07-10)


### Features

* **cycle:** validate `functionalUnit` must not be `1 ha` on `agri-food processor` ([696e4fc](https://gitlab.com/hestia-earth/hestia-data-validation/commit/696e4fcc4874641d3509de7941a27bdc84948bad))
* **practice:** validate should set `value` ([b43cd7f](https://gitlab.com/hestia-earth/hestia-data-validation/commit/b43cd7fab82f8fa50068b5ca13c53a8308b0cf2a))


### Bug Fixes

* **transformation:** handle no `value` on Input or Product ([898e541](https://gitlab.com/hestia-earth/hestia-data-validation/commit/898e5415f5de01d1a5c6c4c7827ca98f5ef08638))

### [0.22.6](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.22.5...v0.22.6) (2023-07-03)


### Features

* **cycle:** validate `animalProduct` with duplicated `units` ([d8d18f1](https://gitlab.com/hestia-earth/hestia-data-validation/commit/d8d18f1b39c74ba243664e650ca0f139ecbfd6ff))


### Bug Fixes

* **cycle:** validate unique products ([3d6fd93](https://gitlab.com/hestia-earth/hestia-data-validation/commit/3d6fd93db6fba859650350e9530399a28f426372))

### [0.22.5](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.22.4...v0.22.5) (2023-06-29)


### Features

* **cycle:** validate `organicFertiliser` with duplicated `units` ([01fee24](https://gitlab.com/hestia-earth/hestia-data-validation/commit/01fee2416de4af98ab98cd6822a94c7cfb5b6282))
* **shared:** validate model results with same `methodTier` ([bfefeee](https://gitlab.com/hestia-earth/hestia-data-validation/commit/bfefeee23ea6b43b0be187f05720ac7a56d5e7c8))

### [0.22.4](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.22.3...v0.22.4) (2023-06-21)


### Features

* **animal:** validate must specify for animal production cycle ([1356cfb](https://gitlab.com/hestia-earth/hestia-data-validation/commit/1356cfbe3c84330433b736715b0a66e090c10f38))
* **practice:** validate `waterRegime` combination with `rice` products ([089f48d](https://gitlab.com/hestia-earth/hestia-data-validation/commit/089f48d6caee4b0aeb71bbb3ad6ad232b114919f))


### Bug Fixes

* **measurement:** handle no `depthUpper` or `depthLower` ([ebad47e](https://gitlab.com/hestia-earth/hestia-data-validation/commit/ebad47e06d414aa4d24670c17b6edb7ec19ef8a3))

### [0.22.3](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.22.2...v0.22.3) (2023-06-19)


### Features

* **completeness:** add `siteType` params in error ([0cdef55](https://gitlab.com/hestia-earth/hestia-data-validation/commit/0cdef55611519982855e374410b06b980b0b311f))
* **practice:** validate sum `waterRegime` is `100%` ([c757efa](https://gitlab.com/hestia-earth/hestia-data-validation/commit/c757efa75aa169a883a55061f9cd1587325e8829))
* **product:** add validate on `liveAnimal` requires `excreta` product ([dd9202d](https://gitlab.com/hestia-earth/hestia-data-validation/commit/dd9202de283183d2848bdc2bd51ea6e71fc40680))


### Bug Fixes

* **input:** use `;` to split values in `mustIncludeId` lookup ([8e69dda](https://gitlab.com/hestia-earth/hestia-data-validation/commit/8e69dda4c21a2e234883a9d1cd2bb009c1ba16d1))
* **practice:** handle practices with values not as numbers ([6dfcf90](https://gitlab.com/hestia-earth/hestia-data-validation/commit/6dfcf902bc321c461e9e7c56631575c6359b4610))

### [0.22.2](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.22.1...v0.22.2) (2023-06-07)


### Bug Fixes

* **requirements:** require schema version `21` ([f62a9b9](https://gitlab.com/hestia-earth/hestia-data-validation/commit/f62a9b9323b33334123a989d69e0f720e0223cd1))
* **transformation:** fix validate previous value from Product ([85654c7](https://gitlab.com/hestia-earth/hestia-data-validation/commit/85654c7d01e3a748de614406405d6a57e6f50337))

### [0.22.1](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.22.0...v0.22.1) (2023-06-02)


### Bug Fixes

* **terms:** set search limit to 10000 ([0fc0d43](https://gitlab.com/hestia-earth/hestia-data-validation/commit/0fc0d43f10e8c5bbf768b41cfa26e5bbb32f12e5))

## [0.22.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.21.0...v0.22.0) (2023-05-31)


### ⚠ BREAKING CHANGES

* Min schema version is `21.0.0`

### Bug Fixes

* update schema to 21 ([6aef2e1](https://gitlab.com/hestia-earth/hestia-data-validation/commit/6aef2e1acfcc35b7ec69a3a4bfad803f66c5446c)), closes [#230](https://gitlab.com/hestia-earth/hestia-data-validation/issues/230) [#231](https://gitlab.com/hestia-earth/hestia-data-validation/issues/231)

## [0.21.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.20.3...v0.21.0) (2023-05-29)


### ⚠ BREAKING CHANGES

* Min schema version required is `20.2.0`

### Features

* **gee:** handle caching of requests ([d0ce452](https://gitlab.com/hestia-earth/hestia-data-validation/commit/d0ce45249ba7b48200fcffdf07b40215de05dc83))
* **input:** only validate inputs using distribution on cropland ([1754534](https://gitlab.com/hestia-earth/hestia-data-validation/commit/1754534c13872dc53540a8bf2f9363436b819cc7))
* **input:** validate irrigation water use using distribution ([3957c43](https://gitlab.com/hestia-earth/hestia-data-validation/commit/3957c43bcbea2a522c819125f4be3884a9ac6340))
* **input:** validate pesticides value using distribution ([de2009e](https://gitlab.com/hestia-earth/hestia-data-validation/commit/de2009e0be9b1f7b524261676d137caeb6c5adca))
* **requirements:** require distribution version above `0.0.11` ([fcb1578](https://gitlab.com/hestia-earth/hestia-data-validation/commit/fcb15783dce3bacb3097742c09cd6f862e3f31d5))
* **requirements:** require schema version `2.2.0` ([ecade76](https://gitlab.com/hestia-earth/hestia-data-validation/commit/ecade76e321db855eb8d53c6e63a7a2601ef279a))


### Bug Fixes

* **distribution:** return positive min values only ([b9a5a09](https://gitlab.com/hestia-earth/hestia-data-validation/commit/b9a5a0914f00f227b7fb7bb164475fffe04b2f31))
* **geojson:** catch error get geometry error ([7332f7b](https://gitlab.com/hestia-earth/hestia-data-validation/commit/7332f7b30abac747cc122c6019e95fab8de3abcd))
* **transformation:** simplify rules about previous inputs and products ([3c09644](https://gitlab.com/hestia-earth/hestia-data-validation/commit/3c096440928ecb918fe7d5781b0d4dba1da38818))

### [0.20.3](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.20.2...v0.20.3) (2023-04-17)


### Bug Fixes

* **validation:** skip validating nodes without data ([4261e57](https://gitlab.com/hestia-earth/hestia-data-validation/commit/4261e57cc5fab86c31054096b31b523ea46a6216))

### [0.20.2](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.20.1...v0.20.2) (2023-04-11)


### Features

* **gee:** return distance when checking coordinates in error ([384f2b4](https://gitlab.com/hestia-earth/hestia-data-validation/commit/384f2b4ef31aa0ecc3eb96ed1f13e1c7169197a0))


### Bug Fixes

* **completeness:** update siteType restriction on `animalFeed` ([5f8e81c](https://gitlab.com/hestia-earth/hestia-data-validation/commit/5f8e81c56c522e459ee8a2d3700d8c61ef69ba97))
* **input:** fix incorrect path on `isAnimalFeed` ([258d4dc](https://gitlab.com/hestia-earth/hestia-data-validation/commit/258d4dc4a7538a078909e1d0fd160017b3bef264))

### [0.20.1](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.20.0...v0.20.1) (2023-04-07)


### Features

* **cycle:** validate `treatment` is set when `experimentDesign` is set ([5140e9c](https://gitlab.com/hestia-earth/hestia-data-validation/commit/5140e9ca5c46bb67170d9ae4331a9e630860e92e))
* **cycle:** validate products do not contain both `liveAnimal` and `animalProduct` ([c326891](https://gitlab.com/hestia-earth/hestia-data-validation/commit/c326891b0cae208b186f85fcc0b6d12979bc439d))
* **impactAssessment:** validate `endDate` equals to cycle `endDate` ([69a1e07](https://gitlab.com/hestia-earth/hestia-data-validation/commit/69a1e07f534e7ef173d953ebd8250c76acb4a210))

## [0.20.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.19.0...v0.20.0) (2023-03-24)


### ⚠ BREAKING CHANGES

* **requirements:** supported schema version is `18`

* **requirements:** update schema to `18` ([1963c9a](https://gitlab.com/hestia-earth/hestia-data-validation/commit/1963c9a670f4d367053fce1586b0bcc4d9edd533))

## [0.19.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.18.0...v0.19.0) (2023-03-15)


### ⚠ BREAKING CHANGES

* **input:** schema min version `17.2.0` required

### Features

* **emission:** add validator `methodTier` should not be "not relevant" ([408354c](https://gitlab.com/hestia-earth/hestia-data-validation/commit/408354c72e7b6da3ee0d0520c47b460888256214))
* **input:** validate feed must specify `fate` field ([25b39a9](https://gitlab.com/hestia-earth/hestia-data-validation/commit/25b39a98f3ef50d004443c81617001d7350610a0))
* **practice:** validate sum of pastureGrass values must be `100` ([606ea37](https://gitlab.com/hestia-earth/hestia-data-validation/commit/606ea378f0f7a3ac3d23067ecdb02bf80dc3b796))

## [0.18.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.17.0...v0.18.0) (2023-03-09)


### ⚠ BREAKING CHANGES

* **requirements:** schema min version `17.1.0` required

### Features

* **completeness:** validate `animalFeed` ([ce5ce37](https://gitlab.com/hestia-earth/hestia-data-validation/commit/ce5ce373d04711cc22b56028c72aa396c58883f8))
* **cycle:** remove `functionalUnit` restriction based on `siteType` ([cbe0abf](https://gitlab.com/hestia-earth/hestia-data-validation/commit/cbe0abfa1d937394148705a37fd007d57af640da))
* **practice:** validate `permanent pasture` should have `pastureGrass` ([396afd5](https://gitlab.com/hestia-earth/hestia-data-validation/commit/396afd5a571c6cbbd147100d09c30e02e6f695b7))
* **requirements:** use schema `17.1.0` ([62b2720](https://gitlab.com/hestia-earth/hestia-data-validation/commit/62b2720b5244e1b40da29bbbf1f5d48295346f60))


### Bug Fixes

* **site:** remove validators on `practices` ([433c25e](https://gitlab.com/hestia-earth/hestia-data-validation/commit/433c25e732ab0f65d6bf264d3ad836fe4a1aa321))

## [0.17.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.16.2...v0.17.0) (2023-02-28)


### ⚠ BREAKING CHANGES

* **measurement:** min `hestia_earth.models` version is now `0.42.0`

### Bug Fixes

* **input:** handle no related impacts ([9de03b4](https://gitlab.com/hestia-earth/hestia-data-validation/commit/9de03b402660abe138fa7d07bef9d25046ca381b))
* **measurement:** fix `spatial` model now under `geospatialDatabase` ([eeda2f9](https://gitlab.com/hestia-earth/hestia-data-validation/commit/eeda2f97bb32ffcb4c9fc337477c375dc3613092))

### [0.16.2](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.16.1...v0.16.2) (2023-02-14)


### Features

* **distribution:** add `VALIDATE_DISTRIBUTION` to toggle feature ([a54c614](https://gitlab.com/hestia-earth/hestia-data-validation/commit/a54c614c62c69a05728ae56e51e75e614c4d97ab))

### [0.16.1](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.16.0...v0.16.1) (2023-02-14)


### Features

* **emission:** add validator model should not be "not relevant" ([4cc1f45](https://gitlab.com/hestia-earth/hestia-data-validation/commit/4cc1f4508b7d4418fe2ad41a8cf0240b0c541ea0))


### Bug Fixes

* **input:** use `product_id` when validating distribution using posterior data ([f0ab798](https://gitlab.com/hestia-earth/hestia-data-validation/commit/f0ab798c9646b8433df22c5ed2c9ab681a18c734)), closes [#207](https://gitlab.com/hestia-earth/hestia-data-validation/issues/207)
* **shared:** handle check dates allow equal dates ([e81bfd8](https://gitlab.com/hestia-earth/hestia-data-validation/commit/e81bfd82730906cdb286f45258c19da2017fbaa3))

## [0.16.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.15.1...v0.16.0) (2023-01-31)


### ⚠ BREAKING CHANGES

* `hestia_earth.models` needs to be installed separately if used.
Use `VALIDATE_MODELS=false` env variable to disable it.

### Features

* **cycle:** validate fertiliser values ([3257e1f](https://gitlab.com/hestia-earth/hestia-data-validation/commit/3257e1f63fbd030c1e7aa06053bb019f15aa3696))


### Bug Fixes

* **measurement:** fix error message when depthUpper/depthLower are required ([621ec24](https://gitlab.com/hestia-earth/hestia-data-validation/commit/621ec24c65b6c08ffaaa32af2063b368fb820bb2))


* make `models` library optional ([55e0351](https://gitlab.com/hestia-earth/hestia-data-validation/commit/55e035166b09e194e4c82073072cd67c7fb24e1a))

### [0.15.1](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.15.0...v0.15.1) (2023-01-19)


### Features

* **cycle:** add validator add fate of crop residue ([4105cd9](https://gitlab.com/hestia-earth/hestia-data-validation/commit/4105cd9dfc19c026634084393fa4f4c60d3ef233))


### Bug Fixes

* **cycle:** require cropResidue practices when complete ([2106fb6](https://gitlab.com/hestia-earth/hestia-data-validation/commit/2106fb63e852d61f403ef0a49e8fca8314d4ff7b))
* **terms:** increase search limit on find `cropResidue` terms ([27eac91](https://gitlab.com/hestia-earth/hestia-data-validation/commit/27eac91822b4d49f49d01fc576b06725c3304ac2))

## [0.15.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.14.5...v0.15.0) (2023-01-04)


### ⚠ BREAKING CHANGES

* **spatial:** min version of `hestia_earth.earth_engine` is `0.2.0` (optional)

* **spatial:** migrate to `hestia_earth.earth_engine` version `0.2.0` ([f6a9cd9](https://gitlab.com/hestia-earth/hestia-data-validation/commit/f6a9cd99eea63cbb0642e5e989baa2ea8e46ca59))

### [0.14.5](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.14.4...v0.14.5) (2022-12-27)


### Bug Fixes

* **utils:** remove check of numpy not working for all versions ([4f6d589](https://gitlab.com/hestia-earth/hestia-data-validation/commit/4f6d5892d32585ee704fd2a9e853a964d180b541))

### [0.14.4](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.14.3...v0.14.4) (2022-12-16)


### Features

* **cycle:** validate list dates are later than `startDate` ([93f6906](https://gitlab.com/hestia-earth/hestia-data-validation/commit/93f69066fd09756016b0ee1653bcaf30c6c5625f))
* **site:** validate list dates are later than `startDate` ([994063b](https://gitlab.com/hestia-earth/hestia-data-validation/commit/994063b83090f097cfca8d54b28446941edfdacc))


### Bug Fixes

* **shared:** change validation level of `area` from `boundary` to `warning` ([1f014fc](https://gitlab.com/hestia-earth/hestia-data-validation/commit/1f014fcb6636dbdbd05c11830661deb1fd408f6b))

### [0.14.3](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.14.2...v0.14.3) (2022-12-14)


### Bug Fixes

* **product:** handle no `value` validating yield ([971dd47](https://gitlab.com/hestia-earth/hestia-data-validation/commit/971dd4799169b81a1b8e2b4acfefaca894bccbbd))

### [0.14.2](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.14.1...v0.14.2) (2022-12-13)


### Features

* **product:** handle error running distribution library ([333a7b9](https://gitlab.com/hestia-earth/hestia-data-validation/commit/333a7b99006eb4e5f99ba0f939a07968c329c8cb))


### Bug Fixes

* **impact assessment:** fix single linked IA per product ([ca80b63](https://gitlab.com/hestia-earth/hestia-data-validation/commit/ca80b639638a2682c60d45a8f1b5bebec93cd0f1))

### [0.14.1](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.14.0...v0.14.1) (2022-12-12)


### Features

* **impact assessment:** validate multiple IA linked to the same Cycle Product ([858e5b1](https://gitlab.com/hestia-earth/hestia-data-validation/commit/858e5b15a08995fb6fdc8051c30ecd41ff096e09))
* **practice:** add validator for `key.units` for `pastureGrass` ([6e7b60a](https://gitlab.com/hestia-earth/hestia-data-validation/commit/6e7b60a04b15088b0ee31daf65efe435aefa7083))
* **product:** validate yield using posterior or prior data ([0016223](https://gitlab.com/hestia-earth/hestia-data-validation/commit/0016223fa1ffbe4c318b4c7850fb4e953ceec4bf))
* **source:** validate `bibliography.year` is before current year ([35da9ff](https://gitlab.com/hestia-earth/hestia-data-validation/commit/35da9ff10a8b22f48cc367bb23bc366d65ada47d))
* **validators:** log node in error ([9976700](https://gitlab.com/hestia-earth/hestia-data-validation/commit/997670093c2f3fe8a303bba743a0ca15c2d46464))


### Bug Fixes

* **impact assessment:** add missing parameter ([afc8486](https://gitlab.com/hestia-earth/hestia-data-validation/commit/afc84860732adf7d3a8853cda591c7b905d051c8))
* **measurement:** handle no value on soil texture ([28a01dd](https://gitlab.com/hestia-earth/hestia-data-validation/commit/28a01dd35d46fd9650583ecdb8f7ae5232726a30))

## [0.14.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.13.0...v0.14.0) (2022-11-29)


### ⚠ BREAKING CHANGES

* schema min version is `14`

* rename `dataComplentess` by `completeness` ([565baa2](https://gitlab.com/hestia-earth/hestia-data-validation/commit/565baa2cc17dbac7026f7c75d9dfcaefd8b81147))

## [0.13.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.12.0...v0.13.0) (2022-11-14)


### ⚠ BREAKING CHANGES

* **measurement:** min models version is `0.36.0`

### Features

* **product:** validate value below 1 for functional unit `1 ha` ([9707f60](https://gitlab.com/hestia-earth/hestia-data-validation/commit/9707f60d434a075e348cce7eca1c65e69ba5ff14))
* **transformation:** validate Product same as Input ([89ddae2](https://gitlab.com/hestia-earth/hestia-data-validation/commit/89ddae263ca99a3043559fa1cbb92d510c4c8d3a))


* **measurement:** validate precipitation instead of rainfall ([88bea79](https://gitlab.com/hestia-earth/hestia-data-validation/commit/88bea7936b7202ef14ceb0f450987046cc868dd5))

## [0.12.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.11.9...v0.12.0) (2022-10-10)


### ⚠ BREAKING CHANGES

* **requirements:** schema min version is `12.0.0` and models min version is `0.35.0`

### Features

* **requirements:** use schema `12.0.0` ([4e0c1af](https://gitlab.com/hestia-earth/hestia-data-validation/commit/4e0c1af3859ca62028eb347584c6a2e0b5b9d984))

### [0.11.9](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.11.8...v0.11.9) (2022-10-04)


### Features

* **impact assessment:** add validators for `endpoints` ([8818245](https://gitlab.com/hestia-earth/hestia-data-validation/commit/881824513d08ac6be389cd15c3d180404971bb15))
* **input:** reduce validation level to `warning` on must include all fertilizers ([32496a6](https://gitlab.com/hestia-earth/hestia-data-validation/commit/32496a6cb3f5629fde9b1cc8a2fe936de42dbb78))


### Bug Fixes

* **shared:** validate percentage value is not a number ([1876af2](https://gitlab.com/hestia-earth/hestia-data-validation/commit/1876af2f549d7df06d09a3ca4f99d235c942c791))

### [0.11.8](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.11.7...v0.11.8) (2022-09-23)

### [0.11.7](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.11.6...v0.11.7) (2022-09-23)

### [0.11.6](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.11.5...v0.11.6) (2022-09-23)


### Features

* **mocking:** add mocking method for search results ([3eca810](https://gitlab.com/hestia-earth/hestia-data-validation/commit/3eca810b8b684155455d5139f3ceaa55ef6a8880))

### [0.11.5](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.11.4...v0.11.5) (2022-09-22)


### Bug Fixes

* **measurement:** require depths only for `soilTexture` ([7f3eda5](https://gitlab.com/hestia-earth/hestia-data-validation/commit/7f3eda5925c4feb9da83880d057c4c8b86a9fefc))

### [0.11.4](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.11.3...v0.11.4) (2022-09-20)


### Features

* **cycle:** handle glass site type ([260be73](https://gitlab.com/hestia-earth/hestia-data-validation/commit/260be7326879ddc9d466c248387748dc328b8665))
* **measurement:** set error instead of warning on mixed depths missing ([0ca3030](https://gitlab.com/hestia-earth/hestia-data-validation/commit/0ca3030157f16a3241bbc0f6a44f81ca5a096200))

### [0.11.3](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.11.2...v0.11.3) (2022-09-06)


### Bug Fixes

* **product:** fix typo in `kg VS` units ([9c7e361](https://gitlab.com/hestia-earth/hestia-data-validation/commit/9c7e36164cc6aa62fa419fa130b77a53ee581349))

### [0.11.2](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.11.1...v0.11.2) (2022-09-06)


### Features

* **dataCompleteness:** validate `material` if used with `fuel` ([59e8659](https://gitlab.com/hestia-earth/hestia-data-validation/commit/59e86594fd00bd218409c3ebf88f5e241f5f44b2))
* **product:** handle different `units` while validating `excreta` ([25621ed](https://gitlab.com/hestia-earth/hestia-data-validation/commit/25621edd944068d90637e629cb02232b86a87ac3))
* **product:** handle error `excreta` term is not allowed ([5ca4378](https://gitlab.com/hestia-earth/hestia-data-validation/commit/5ca4378299fa7b7091a67e8dfb62ae30171dce56))

### [0.11.1](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.11.0...v0.11.1) (2022-08-23)


### Features

* **practice:** validate `liveAnimal` should set `system` as `Practice` ([741ba83](https://gitlab.com/hestia-earth/hestia-data-validation/commit/741ba837d620d796f9835b7810891a07acf96a93))
* **property:** validate `volatileSolidsContent` value from units ([cb0a40c](https://gitlab.com/hestia-earth/hestia-data-validation/commit/cb0a40c073bc2b8f6f3a4af1173d2c4597cc04f9))

## [0.11.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.10.14...v0.11.0) (2022-07-29)


### ⚠ BREAKING CHANGES

* **product:** now requiring version `0.6.0` of the glossary lookups

### Features

* **indicator:** validate land transformation below land occupation ([3de5d54](https://gitlab.com/hestia-earth/hestia-data-validation/commit/3de5d54ca77eeaf5e5234d9a4570d71d2777af8d))
* **transformation:** validate all emissions have a linked Cycle emission ([dcbe98b](https://gitlab.com/hestia-earth/hestia-data-validation/commit/dcbe98bf95d4e003822a5d14f33dbcfcd12a254b))


### Bug Fixes

* **product:** rename `excreta` lookup to `excretaKgNTermId` ([e7c1f06](https://gitlab.com/hestia-earth/hestia-data-validation/commit/e7c1f063efdd0157ebe8d7159818d4ed82dc86bc))

### [0.10.14](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.10.13...v0.10.14) (2022-07-18)


### Bug Fixes

* **utils:** skip existing linked node if not found ([e158ba6](https://gitlab.com/hestia-earth/hestia-data-validation/commit/e158ba6c0075258eefaca1df79c23753240830fc))

### [0.10.13](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.10.12...v0.10.13) (2022-07-15)


### Bug Fixes

* **utils:** skip existing linked node if not found ([ea0bd05](https://gitlab.com/hestia-earth/hestia-data-validation/commit/ea0bd055e99da1e55aaea90b91eae0e4f3b92383))

### [0.10.12](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.10.11...v0.10.12) (2022-07-14)


### Features

* **impact assessment:** validate Product exist in linked Cycle ([6267069](https://gitlab.com/hestia-earth/hestia-data-validation/commit/6267069c5dca8143f1b8c4acd9cd72ae2d38ee7d))
* **measurement:** add `model` used to calculate measurement ([e53d387](https://gitlab.com/hestia-earth/hestia-data-validation/commit/e53d3875d9565d8a69df0133dedf1ecfc248b3ef))


### Bug Fixes

* **cycle:** soft validate emissions required `inputs` ([6f380e2](https://gitlab.com/hestia-earth/hestia-data-validation/commit/6f380e27e8d85526d2aa373b7ca0c3b9491e343b))

### [0.10.11](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.10.10...v0.10.11) (2022-07-08)


### Bug Fixes

* **emission:** allow setting empty linked terms ([8ea477c](https://gitlab.com/hestia-earth/hestia-data-validation/commit/8ea477cf46cf23fbf17d351aec7044dbb6db6656))

### [0.10.10](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.10.9...v0.10.10) (2022-07-08)


### Features

* **shared:** set threshold on `boundary` / `area` to `0.05` ([f054c31](https://gitlab.com/hestia-earth/hestia-data-validation/commit/f054c31f477dd05df6643b7943c20f9da847057d))


### Bug Fixes

* **measurement:** allow `depthUpper` to equal `depthLower` ([1b5c66a](https://gitlab.com/hestia-earth/hestia-data-validation/commit/1b5c66a5f4ce0208d3de6877ef828d02a563f0c0))

### [0.10.9](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.10.8...v0.10.9) (2022-07-04)


### Features

* **cycle:** validate `coverCrop` practices/products ([4f35532](https://gitlab.com/hestia-earth/hestia-data-validation/commit/4f35532ccdf82e5cb4c61783f6c57ae67de7ebed))
* **emission:** validate linked `inputs` are specified in the `Cycle` ([790281e](https://gitlab.com/hestia-earth/hestia-data-validation/commit/790281e668744537ed58407c60c8081d5c6324f3))
* **emission:** validate linked `transformation` is specified in the `Cycle` ([5dea632](https://gitlab.com/hestia-earth/hestia-data-validation/commit/5dea6327d74652a2bf72c51e8de54ea9e9b95374))
* **site:** validate `min`/`max` included in lookup values ([cec3516](https://gitlab.com/hestia-earth/hestia-data-validation/commit/cec35167f8bc3422dcb2be2f13ed29e414d5a140))
* **transformation:** validate first transformation has an input as product of Cycle ([909d9b8](https://gitlab.com/hestia-earth/hestia-data-validation/commit/909d9b8278d67167639570d4ab6c46cffefd0dff))
* validate `value` is between `min`/`max` uploaded value ([200ed7f](https://gitlab.com/hestia-earth/hestia-data-validation/commit/200ed7f1f86b8204373536022de199fc51f7e662))

### [0.10.8](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.10.7...v0.10.8) (2022-06-28)


### Features

* **dataCompleteness:** validate `animalFeed=True` on `cropland` ([00a24ad](https://gitlab.com/hestia-earth/hestia-data-validation/commit/00a24ad5dba7a5ad7eb514b99851e0809149acb3))


### Bug Fixes

* **cycle:** allow unspecify `otherSitesDuration` ([aea8500](https://gitlab.com/hestia-earth/hestia-data-validation/commit/aea850096268d02e32af7431a3edf672b749e5dd)), closes [#140](https://gitlab.com/hestia-earth/hestia-data-validation/issues/140)
* **organisation:** remove max-size validation ([c32742d](https://gitlab.com/hestia-earth/hestia-data-validation/commit/c32742d292b2819448a3ed60114e8b67a3b8f41b))

### [0.10.7](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.10.6...v0.10.7) (2022-06-02)


### Bug Fixes

* **requirements:** add missing `hestia_earth.earth_engine` dependency ([059032d](https://gitlab.com/hestia-earth/hestia-data-validation/commit/059032dfeeee6683c9c92dfcb1bd1d6caf0c456b))

### [0.10.6](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.10.5...v0.10.6) (2022-04-20)


### Features

* **shared:** validate private data without a `Source` ([a52314d](https://gitlab.com/hestia-earth/hestia-data-validation/commit/a52314d1bd9bab3fe0cba7613bfbdc4a0e0b3373))


### Bug Fixes

* **measurement:** fix wrong message on missing fields ([6c7dbb4](https://gitlab.com/hestia-earth/hestia-data-validation/commit/6c7dbb4631c5ae9da513daeb9cb03bf5a3f00dc6))
* **measurements:** allow no `startDate`/`endDate` if same as `Site` ([892ef81](https://gitlab.com/hestia-earth/hestia-data-validation/commit/892ef819a4b9de4da639841c3357c0db37a0e183))

### [0.10.5](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.10.4...v0.10.5) (2022-04-06)


### Bug Fixes

* **product:** allow no eva or revenue when product value is `0` ([250bbfb](https://gitlab.com/hestia-earth/hestia-data-validation/commit/250bbfbf59b93bd5d69e98cb44fffa5cf0b36b06))

### [0.10.4](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.10.3...v0.10.4) (2022-04-06)


### Bug Fixes

* **product:** set 0 value validation on `value` field ([4591f8b](https://gitlab.com/hestia-earth/hestia-data-validation/commit/4591f8b2175f2b000a801d952308d14145206fb5))

### [0.10.3](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.10.2...v0.10.3) (2022-04-05)


### Bug Fixes

* **shared:** handle `Geometry has too many edges` error on GEE ([fa41128](https://gitlab.com/hestia-earth/hestia-data-validation/commit/fa41128e73bcb5f328e35ae3b93ebdda3be7b9d1))

### [0.10.2](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.10.1...v0.10.2) (2022-04-01)


### Bug Fixes

* **shared:** only check `boundary`/`region` size if no `coordinates` ([7940d57](https://gitlab.com/hestia-earth/hestia-data-validation/commit/7940d575bba219e557f0e33c130f0d86d994344e))

### [0.10.1](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.10.0...v0.10.1) (2022-03-28)


### Features

* handle validation using nested `Site` ([a977f9e](https://gitlab.com/hestia-earth/hestia-data-validation/commit/a977f9eec4be25ddd1c1dd12014883ec90678f0d))

## [0.10.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.9.3...v0.10.0) (2022-03-19)


### ⚠ BREAKING CHANGES

* **gee:** `GEE_API_ENABLED` env renamed to `VALIDATE_SPATIAL`

### Bug Fixes

* **gee:** skip validate size if not enabled ([51f0375](https://gitlab.com/hestia-earth/hestia-data-validation/commit/51f0375cf54b619a163959fe4333eda073b9fb0f))

### [0.9.3](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.9.2...v0.9.3) (2022-03-17)


### Bug Fixes

* **shared:** handle no region for site ([c1ba5f5](https://gitlab.com/hestia-earth/hestia-data-validation/commit/c1ba5f5a3b0a43bc3f2637e28bacb929a2665e1d))

### [0.9.2](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.9.1...v0.9.2) (2022-03-17)


### Features

* **gee:** validate max size for `country`, `region` and `boundary` ([c384340](https://gitlab.com/hestia-earth/hestia-data-validation/commit/c3843408c9a2af8ff1efb9ca6adcb3b587eaa3d3))
* **product:** validate value `0` ([68cc5f2](https://gitlab.com/hestia-earth/hestia-data-validation/commit/68cc5f2dbfd05e84e3542eaea391290d760fa6e2))

### [0.9.1](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.9.0...v0.9.1) (2022-02-28)


### Bug Fixes

* check privacy of existing sources ([6e425d3](https://gitlab.com/hestia-earth/hestia-data-validation/commit/6e425d3859ea90ce6cf1da5d0a1e179ca02ce4d3))

## [0.9.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.8.8...v0.9.0) (2022-02-24)


### ⚠ BREAKING CHANGES

* **gee:** require engine model version above `0.24.0`

### Features

* **measurements:** add `params.term` on invalid texture error ([c3abb45](https://gitlab.com/hestia-earth/hestia-data-validation/commit/c3abb45d5dcb2749103535d3d009f5977462bb10))


* **gee:** use earth engine library instead of API ([0102681](https://gitlab.com/hestia-earth/hestia-data-validation/commit/0102681282cd08d1ccfa7d1fc3afbd3754d14e16))

### [0.8.8](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.8.7...v0.8.8) (2022-02-14)


### Bug Fixes

* fix validation on date must be before today ([6aad5d2](https://gitlab.com/hestia-earth/hestia-data-validation/commit/6aad5d2250e89c043f1e1e97cddb4b0a36ed9eb3))

### [0.8.7](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.8.6...v0.8.7) (2022-01-17)


### Features

* **property:** raise error check default value property not found ([1ce60c1](https://gitlab.com/hestia-earth/hestia-data-validation/commit/1ce60c153d95b75baa0a4c7807e9901f40b5f04b))

### [0.8.6](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.8.5...v0.8.6) (2021-10-19)


### Features

* **input:** validate not creating related loop ([8c0b73e](https://gitlab.com/hestia-earth/hestia-data-validation/commit/8c0b73ef5c4b06d4390f1ae4206f9e9659db2806))

### [0.8.5](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.8.4...v0.8.5) (2021-10-10)


### Bug Fixes

* **practice:** only account for `unique` tillage practices ([1ce138f](https://gitlab.com/hestia-earth/hestia-data-validation/commit/1ce138f38decce214ac6347a2296132c39d45dde))

### [0.8.4](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.8.3...v0.8.4) (2021-09-27)


### Bug Fixes

* **practice:** handle no tillage practice correctly ([9b974e2](https://gitlab.com/hestia-earth/hestia-data-validation/commit/9b974e2cdc3e06cb8520ddb81956b4e74ba6509d))

### [0.8.3](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.8.2...v0.8.3) (2021-09-27)


### Features

* **cycle:** validate 1 `primary` product max ([94b937a](https://gitlab.com/hestia-earth/hestia-data-validation/commit/94b937a9a4b619e925d2f93f7b22b0c531205c90))
* **practice:** validate `noTillage` used with tillage operation ([c4e81bd](https://gitlab.com/hestia-earth/hestia-data-validation/commit/c4e81bd80a8cf20d69973a43ac2864a5768ae01a))
* **practice:** validate should set tillage for cropland ([cc7610d](https://gitlab.com/hestia-earth/hestia-data-validation/commit/cc7610d6c0ef24b5f7ac81eaa9ab2f28ba22ea08))
* **practice:** validate tillage values ([2065a41](https://gitlab.com/hestia-earth/hestia-data-validation/commit/2065a41d9ee65f2fc069c467c524ea2802b91a1d))

### [0.8.2](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.8.1...v0.8.2) (2021-08-31)


### Features

* **input:** validate `country` property is a country ([80e1810](https://gitlab.com/hestia-earth/hestia-data-validation/commit/80e1810a638e98a5f850b9faeb0122a5c981d32d))
* **practice:** validate `excretaManagement` requires `excreta` Input ([fc4b330](https://gitlab.com/hestia-earth/hestia-data-validation/commit/fc4b33070a75982098decf98fcf124ed74369be5))
* **property:** add `threshold` param ([c18f1ad](https://gitlab.com/hestia-earth/hestia-data-validation/commit/c18f1ad2900e1d502302a351f8074bada99d2449))

### [0.8.1](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.8.0...v0.8.1) (2021-08-29)


### Features

* add `threshold` params ([6d0bcc4](https://gitlab.com/hestia-earth/hestia-data-validation/commit/6d0bcc451b09867704614820245aa1b7fe3aa5de))
* **shared:** add validator max area size 5000km2 ([f4da36c](https://gitlab.com/hestia-earth/hestia-data-validation/commit/f4da36c355ced4e0ade0320932f7b10afd1afa44))

## [0.8.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.7.6...v0.8.0) (2021-08-12)


### ⚠ BREAKING CHANGES

* use schema min `6.5.0`

### Features

* use schema `6.5.0` minimum ([a2f3673](https://gitlab.com/hestia-earth/hestia-data-validation/commit/a2f367387b647716c63863ca568c3926d98af9ee))

### [0.7.6](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.7.5...v0.7.6) (2021-08-11)


### Bug Fixes

* **cycle:** avoid setting `site` on cycle ref ([ebdeb7e](https://gitlab.com/hestia-earth/hestia-data-validation/commit/ebdeb7e4666de21cddd5091330d1273da346a63f)), closes [#97](https://gitlab.com/hestia-earth/hestia-data-validation/issues/97)
* **property:** skip check `valueType` if empty ([83adea4](https://gitlab.com/hestia-earth/hestia-data-validation/commit/83adea440bf1e2f8839f46cf7355645a17f2dd9a)), closes [#96](https://gitlab.com/hestia-earth/hestia-data-validation/issues/96)

### [0.7.5](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.7.4...v0.7.5) (2021-08-11)


### Features

* **cycle:** set `site` linked node to run models ([0d57bc4](https://gitlab.com/hestia-earth/hestia-data-validation/commit/0d57bc4e4de031f4ca56e0d53f2459bfe725dc89))
* **measurement:** add soft validation should set depths ([ea940a7](https://gitlab.com/hestia-earth/hestia-data-validation/commit/ea940a76c9af6d84a811d856d271301eef9123f4))


### Bug Fixes

* **indicator:** ignore `aggregatedModels` in allowed values ([8cef5aa](https://gitlab.com/hestia-earth/hestia-data-validation/commit/8cef5aa0fa68005664e9c50a05f4e0fec7d09a20))

### [0.7.4](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.7.3...v0.7.4) (2021-08-02)


### Features

* **indicator:** add `term` and `model` to error ([15c384b](https://gitlab.com/hestia-earth/hestia-data-validation/commit/15c384be27be010aa4f70cc8d11c54172b42e69c))
* **measurement:** handle no `value` validate with model ([03e7869](https://gitlab.com/hestia-earth/hestia-data-validation/commit/03e78698f661d9378eab58ab5a6ac92f916f0dae))


### Bug Fixes

* **product:** fix dataPath `value` empty to show warning ([0c9523e](https://gitlab.com/hestia-earth/hestia-data-validation/commit/0c9523e6d736b4b957fac321152409a79a83e7ec))

### [0.7.3](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.7.2...v0.7.3) (2021-07-30)


### Features

* **property:** validate `type` of `value` ([92c3290](https://gitlab.com/hestia-earth/hestia-data-validation/commit/92c329011a497248c45683f300fd8458d17a6d1a))


### Bug Fixes

* **shared:** skip validate model if no `value` is set ([c3bd1a4](https://gitlab.com/hestia-earth/hestia-data-validation/commit/c3bd1a47b1dae8f45bac3dff2687e92a457fa57b))

### [0.7.2](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.7.1...v0.7.2) (2021-07-26)


### Features

* **measurement:** validate value length max 1 for some measurements ([08eac30](https://gitlab.com/hestia-earth/hestia-data-validation/commit/08eac3080806fb179965859b3ab7f462e7db057c))

### [0.7.1](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.7.0...v0.7.1) (2021-07-25)


### Features

* skip duplicate checks for `ImpactAssessment` ([d169d34](https://gitlab.com/hestia-earth/hestia-data-validation/commit/d169d34e868439108c16ea5efef174904e290c72)), closes [#87](https://gitlab.com/hestia-earth/hestia-data-validation/issues/87)


### Bug Fixes

* **shared:** fix value average on list of string ([c060a74](https://gitlab.com/hestia-earth/hestia-data-validation/commit/c060a7484916e0f7245f7499c1dab6d7076a472a)), closes [#88](https://gitlab.com/hestia-earth/hestia-data-validation/issues/88)

## [0.7.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.6.2...v0.7.0) (2021-07-23)


### ⚠ BREAKING CHANGES

* **practice:** use schema min `6.0.0`

* **practice:** get value as array ([30c250e](https://gitlab.com/hestia-earth/hestia-data-validation/commit/30c250e10e3b49d8b49deb6bcc6eb1141d8b0335))

### [0.6.2](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.6.1...v0.6.2) (2021-07-23)


### Bug Fixes

* **measurement:** handle no model result ([f7193d3](https://gitlab.com/hestia-earth/hestia-data-validation/commit/f7193d3291a9e71d443c03c21826c36c80bbe541))

### [0.6.1](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.6.0...v0.6.1) (2021-07-23)


### Features

* **indicator:** validate `characterisedIndicator` use with `methodModel` ([8336586](https://gitlab.com/hestia-earth/hestia-data-validation/commit/83365869faac0ffea26eee40788c5f746b6acddb))
* **measurement:** validate `rainfallAnnual` value from GEE ([9689f7f](https://gitlab.com/hestia-earth/hestia-data-validation/commit/9689f7f27adaa5f94ea3b9c1e24de393ebb6afe3))
* **product:** validate `excreta` is not too generic ([60208a6](https://gitlab.com/hestia-earth/hestia-data-validation/commit/60208a6f71bf2908b194180b6b79baf39e63d146))

## [0.6.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.5.4...v0.6.0) (2021-07-22)


### ⚠ BREAKING CHANGES

* use schema min `5.3.0`

### Features

* use schema `5.3.0` ([2532c63](https://gitlab.com/hestia-earth/hestia-data-validation/commit/2532c6320b1273a3a5b533b4725937947fe7d615))

### [0.5.4](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.5.3...v0.5.4) (2021-07-20)

### [0.5.3](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.5.2...v0.5.3) (2021-07-18)


### Features

* add logging system ([b323b41](https://gitlab.com/hestia-earth/hestia-data-validation/commit/b323b41ab819ac139aae2b8f5fd1c3d0a965a6b1))
* **measurement:** validate requires `startDate` and `endDate` ([55fe059](https://gitlab.com/hestia-earth/hestia-data-validation/commit/55fe059befa9736ef24f3ac1474ca7454fb27e83))
* skip validate duplicates for `Actor` ([11e9018](https://gitlab.com/hestia-earth/hestia-data-validation/commit/11e90189a7fc93d8b598d3c29272f1f963d9ca85)), closes [#81](https://gitlab.com/hestia-earth/hestia-data-validation/issues/81)

### [0.5.2](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.5.1...v0.5.2) (2021-07-16)

### [0.5.1](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.5.0...v0.5.1) (2021-07-15)

## [0.5.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.4.1...v0.5.0) (2021-07-15)


### ⚠ BREAKING CHANGES

* **requirements:** schema version is `5.0.0` minimum

### Features

* **cycle:** validate unique `excretaManagement` as `Practice` ([6a1e02c](https://gitlab.com/hestia-earth/hestia-data-validation/commit/6a1e02c126a96ddf738daec129b3cb2aa009bf90))
* **requirements:** use schema `5.0.0` minimum ([aef9fe1](https://gitlab.com/hestia-earth/hestia-data-validation/commit/aef9fe1811a0147b48b8098663e33bdbf639f9a7))
* **transformation:** validate `input` value based on previous identical `product` ([5dc4c12](https://gitlab.com/hestia-earth/hestia-data-validation/commit/5dc4c12131bea668f4480b76429e9086065c8bc9))
* **transformation:** validate keys not present in first transformation ([a1f1237](https://gitlab.com/hestia-earth/hestia-data-validation/commit/a1f1237fc175e7702ee81082a1f26a7c9e0208ea)), closes [#78](https://gitlab.com/hestia-earth/hestia-data-validation/issues/78)
* validate duplicate nodes same `type` different `id` ([d0d3f1e](https://gitlab.com/hestia-earth/hestia-data-validation/commit/d0d3f1ebb8b5c92bf3caf479091058c5a6d9b980))

### [0.4.1](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.4.0...v0.4.1) (2021-07-02)


### Bug Fixes

* **cycle:** handle no `siteDuration` ([5415c60](https://gitlab.com/hestia-earth/hestia-data-validation/commit/5415c608787ae2a1f066eda72fca1806262d8be8))

## [0.4.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.3.7...v0.4.0) (2021-07-02)


### ⚠ BREAKING CHANGES

* **requirements:** works on schema above `4.5.0`

### Features

* **cycle:** validate `siteDuration` with and without `otherSites` ([0ba22af](https://gitlab.com/hestia-earth/hestia-data-validation/commit/0ba22af336001fc74d4887ebe5c88e703d5b764c))
* **requirements:** use schema to `4.5.0` ([90a6787](https://gitlab.com/hestia-earth/hestia-data-validation/commit/90a6787d595ff048198475a993bf1a18e92949ad))


### Bug Fixes

* **shared:** handle property without a `Term` ([3cdfcda](https://gitlab.com/hestia-earth/hestia-data-validation/commit/3cdfcda7da18f11ab818fd63eb835229baf22877))

### [0.3.7](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.3.6...v0.3.7) (2021-06-27)


### Features

* **shared:** handle allowed exceptions for property default values ([b4b2960](https://gitlab.com/hestia-earth/hestia-data-validation/commit/b4b2960408572076e6067d85cf895c9a6dd345d7))
* **shared:** validate `endDate` and `startDate` same format ([41456b9](https://gitlab.com/hestia-earth/hestia-data-validation/commit/41456b9c51cd91a57b9df347cb432a5958ebd25e))


### Bug Fixes

* **shared:** handle validate model config no value ([2146ca0](https://gitlab.com/hestia-earth/hestia-data-validation/commit/2146ca0899fac3b865c3362c74e9dc50154e3483))

### [0.3.6](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.3.5...v0.3.6) (2021-06-17)


### Bug Fixes

* **input:** handle no other term ids ([53fed2c](https://gitlab.com/hestia-earth/hestia-data-validation/commit/53fed2c044855abcf60e4d812f6cace1165e7677))

### [0.3.5](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.3.4...v0.3.5) (2021-06-17)


### Features

* **cycle:** validate `aboveGroundCropResidueTotal` using model ([04f729e](https://gitlab.com/hestia-earth/hestia-data-validation/commit/04f729e8112b895e60f2276f12976db6dbe1722b))
* **input:** validate required ids using `mustIncludeId` lookup column ([fa9b1ea](https://gitlab.com/hestia-earth/hestia-data-validation/commit/fa9b1ea3e998f4b29a0e1bd55330db10735a0256))
* **requirements:** use schema 4.2.0 ([9acc571](https://gitlab.com/hestia-earth/hestia-data-validation/commit/9acc5715c0ff00cd80f46b2df26510af59fb5f68))
* **shared:** validate country level 0 ([99ff724](https://gitlab.com/hestia-earth/hestia-data-validation/commit/99ff724cf23ae94e3a25ac7d045326383408e159))
* **shared:** validate region GADM level above `0` ([fcfb2eb](https://gitlab.com/hestia-earth/hestia-data-validation/commit/fcfb2eb33fb97b28d840bdd3e78c6fc814f2f540))


### Bug Fixes

* **infrastructure:** fix `lifespan` index ([1c343bb](https://gitlab.com/hestia-earth/hestia-data-validation/commit/1c343bb8d3e6748b37edfcfe94b7e01f993747ba))
* **measurements:** fix `depthLower` validation index ([0485c46](https://gitlab.com/hestia-earth/hestia-data-validation/commit/0485c46c2608d8f08b913c783f1b094c2f21e960))
* **shared:** skip warning percentage if value = `0` ([5fad728](https://gitlab.com/hestia-earth/hestia-data-validation/commit/5fad72883f4c2beaad3aeaaa8d78a74a24d6a091)), closes [#65](https://gitlab.com/hestia-earth/hestia-data-validation/issues/65)

### [0.3.4](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.3.3...v0.3.4) (2021-06-12)


### Bug Fixes

* **shared:** handle model return empty result list ([d312d26](https://gitlab.com/hestia-earth/hestia-data-validation/commit/d312d26a5e3b9ce1a31673a3d8fe604feeabfea1))

### [0.3.3](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.3.2...v0.3.3) (2021-06-11)


### Features

* **shared:** add env variable to disable validation of models ([67e2b64](https://gitlab.com/hestia-earth/hestia-data-validation/commit/67e2b64a7e295826123bbddfdf1f28be49e8abfa))

### [0.3.2](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.3.1...v0.3.2) (2021-06-11)


### Features

* **emission:** validate method model result using engine models ([e004810](https://gitlab.com/hestia-earth/hestia-data-validation/commit/e004810e52e11195dc7c4affceca61af575ed9fa))
* **impact assessment:** validate `impacts` method model result using engine models ([f17c8b2](https://gitlab.com/hestia-earth/hestia-data-validation/commit/f17c8b2d0e9e0c7d017f67adc432bc7b95818cb2))

### [0.3.1](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.3.0...v0.3.1) (2021-06-03)


### Bug Fixes

* **shared:** handle comparing min/max with only one of them ([f41d182](https://gitlab.com/hestia-earth/hestia-data-validation/commit/f41d182062e59a58aebe9639799eee831b8b31ed))

## [0.3.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.2.1...v0.3.0) (2021-06-02)


### ⚠ BREAKING CHANGES

* **requirements:** require version `4.0.0` of the schema

### Features

* validate `properties` default values delta less than 25% ([935a5fa](https://gitlab.com/hestia-earth/hestia-data-validation/commit/935a5fa81faef0ab29a738daf12c82b8ce321458))
* validate dates earlier than today for `startDate` / `endDate` ([90e3a62](https://gitlab.com/hestia-earth/hestia-data-validation/commit/90e3a624d8e24837deecdb17c7f8ee69008f44cc))
* **requirements:** use schema `3.7.0` ([dcfef43](https://gitlab.com/hestia-earth/hestia-data-validation/commit/dcfef43a9175b1cba62e79cf0942ab722482aea6))
* **requirements:** use schema `4.0.0` ([6d97591](https://gitlab.com/hestia-earth/hestia-data-validation/commit/6d975911639191b995bf34c3d3f3fa12b7998790))


### Bug Fixes

* **cycle:** fix error in key `inputs` ([0f56c2a](https://gitlab.com/hestia-earth/hestia-data-validation/commit/0f56c2a2c8b4d56c6af4cf9f443137b4407f30de))

### [0.2.1](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.2.0...v0.2.1) (2021-05-21)


### Features

* **validators:** add env var to enable validation on existing nodes ([ea994ad](https://gitlab.com/hestia-earth/hestia-data-validation/commit/ea994ad0a2c704827785b5eb2de97b8f495bfc3c))

## [0.2.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.30...v0.2.0) (2021-05-21)


### Features

* **bin:** add script to validate data from folder ([36ae36f](https://gitlab.com/hestia-earth/hestia-data-validation/commit/36ae36fe9c3e570e0336b27a0511dedbdd751ac4))
* **gee:** add env var to disable API ([0ee51b7](https://gitlab.com/hestia-earth/hestia-data-validation/commit/0ee51b73b7aeec50138fcfa62a0fa128cf77d380))

### [0.1.30](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.29...v0.1.30) (2021-05-19)


### Bug Fixes

* **practice:** handle no practice for fallow period ([ff15af2](https://gitlab.com/hestia-earth/hestia-data-validation/commit/ff15af225b3a7bfb9079f64c876ae304ced1e873))

### [0.1.29](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.28...v0.1.29) (2021-05-18)


### Features

* **practice:** validate `longFallowPeriod` less than 5 years ([37bb3bb](https://gitlab.com/hestia-earth/hestia-data-validation/commit/37bb3bb5857860940fc57041e2ec02b488a3f8e3))


### Bug Fixes

* rename percent units to `%` ([0e4b166](https://gitlab.com/hestia-earth/hestia-data-validation/commit/0e4b166919e29837b63ce95e4a81f275a171e32c)), closes [#52](https://gitlab.com/hestia-earth/hestia-data-validation/issues/52)

### [0.1.28](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.27...v0.1.28) (2021-05-13)


### Features

* **cycle:** allow different `operation` for identical `inputs` ([3c31d3d](https://gitlab.com/hestia-earth/hestia-data-validation/commit/3c31d3d0b09fde9870a3b76b17d780dd416fef8c))
* **cycle:** validate sum above ground / below ground for cropland ([61d2c97](https://gitlab.com/hestia-earth/hestia-data-validation/commit/61d2c97355340c608e1cbb54f9207ba06778979b))

### [0.1.27](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.26...v0.1.27) (2021-05-07)


### Features

* **cycle:** validate duplicate `term.[@id](https://gitlab.com/id)` in `transformations` ([ff297ea](https://gitlab.com/hestia-earth/hestia-data-validation/commit/ff297ea4b5b28252f7fc96a966d0ef39cc1c9564))
* **requirements:** use schema 3.3.0 ([670daf5](https://gitlab.com/hestia-earth/hestia-data-validation/commit/670daf5042ee04ca381ceb29120e23fb1c90f999))
* **transformation:** validate previous transformation index ([e2a0454](https://gitlab.com/hestia-earth/hestia-data-validation/commit/e2a0454d168f62129ac269df7d6be97cb236d5e7))

### [0.1.26](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.25...v0.1.26) (2021-05-06)


### Features

* **requirements:** use schema 3.2.0 ([aa50712](https://gitlab.com/hestia-earth/hestia-data-validation/commit/aa50712b30e2d937b46b94ebb917c7b709a163cc))


### Bug Fixes

* **cycle:** replace `relDays` with `dates` ([ff4fcdb](https://gitlab.com/hestia-earth/hestia-data-validation/commit/ff4fcdb24b0d38209201427d057eae3eac14c212))

### [0.1.25](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.24...v0.1.25) (2021-04-22)


### Features

* **cycles:** validate sum of `aboveGroundCropResidue` values ([4502ee8](https://gitlab.com/hestia-earth/hestia-data-validation/commit/4502ee8398eac619ca03edd7ba878d30258843f0))
* **product:** set warning when `value` is not set ([86faade](https://gitlab.com/hestia-earth/hestia-data-validation/commit/86faade7365ca3045d53f010346d13d3acd47c40))
* **requirements:** use schema 3.0.0 ([f6bbadf](https://gitlab.com/hestia-earth/hestia-data-validation/commit/f6bbadf766c692891a5f20d43530a119500860fa))

### [0.1.24](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.23...v0.1.24) (2021-03-19)


### Features

* **cycle:** validate `defaultSource` same privacy ([b219e2f](https://gitlab.com/hestia-earth/hestia-data-validation/commit/b219e2f8b83a3b6cc1e7bcfa3824727b7340efa2)), closes [#36](https://gitlab.com/hestia-earth/hestia-data-validation/issues/36)
* **impact assessment:** validate `source` same privacy ([0185f69](https://gitlab.com/hestia-earth/hestia-data-validation/commit/0185f6942564e7fe290c3cf7886511d33de38ecd)), closes [#36](https://gitlab.com/hestia-earth/hestia-data-validation/issues/36)
* **site:** validate `defaultSource` same privacy ([4680466](https://gitlab.com/hestia-earth/hestia-data-validation/commit/4680466643899943833296e3e16f590fdcc87a65)), closes [#36](https://gitlab.com/hestia-earth/hestia-data-validation/issues/36)


### Bug Fixes

* **cycle:** get linked site to validate `functionalUnitMeasure` ([e665ec9](https://gitlab.com/hestia-earth/hestia-data-validation/commit/e665ec9d4a29cf726b5ad2db19cdb3f36108dd90))
* **cycle datacompleteness:** validate manureManagement with site cropland ([6182adf](https://gitlab.com/hestia-earth/hestia-data-validation/commit/6182adf0b2d9065c8e86af3bc8f98c91c3a774c7))

### [0.1.23](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.22...v0.1.23) (2021-03-17)


### Features

* **validators:** valid empty fields on every node ([20b44ae](https://gitlab.com/hestia-earth/hestia-data-validation/commit/20b44ae48615857d1bc62cfe4d3e93c9835007bc))


### Bug Fixes

* **measurements:** ignore min/max if not defined ([5bdeb74](https://gitlab.com/hestia-earth/hestia-data-validation/commit/5bdeb7456594828b96a6548d8ac2353bc46ace48)), closes [#37](https://gitlab.com/hestia-earth/hestia-data-validation/issues/37)
* **validators:** only validate uploaded nodes ([828fb8e](https://gitlab.com/hestia-earth/hestia-data-validation/commit/828fb8ed37bd983de8c38c5b3c7341aa55be1c84))

### [0.1.22](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.21...v0.1.22) (2021-03-02)


### Bug Fixes

* **data completeness:** still not working ([e11fec6](https://gitlab.com/hestia-earth/hestia-data-validation/commit/e11fec699c360e0df902aec4f6a97972f01c6f60))

### [0.1.21](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.20...v0.1.21) (2021-03-02)


### Bug Fixes

* **data completeness:** only check for boolean values ([a9c7be7](https://gitlab.com/hestia-earth/hestia-data-validation/commit/a9c7be704d273c6e554d60f833f39065d302680b))

### [0.1.20](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.19...v0.1.20) (2021-03-01)


### Features

* **measurement:** validate unique terms ([d8fc7b6](https://gitlab.com/hestia-earth/hestia-data-validation/commit/d8fc7b6ae5b0e5200d9171bfd7522f4c964b5282)), closes [#35](https://gitlab.com/hestia-earth/hestia-data-validation/issues/35)

### [0.1.19](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.18...v0.1.19) (2021-03-01)


### Bug Fixes

* **cycle:** skip warning dataCompleteness related cycle ([a79daa4](https://gitlab.com/hestia-earth/hestia-data-validation/commit/a79daa43623c051a511ef919fc6270f2cf4dfcaf))
* **shared:** handle value average as single value ([512e483](https://gitlab.com/hestia-earth/hestia-data-validation/commit/512e483493dbadc3d009e386c1edf50d62d3f054))

### [0.1.18](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.17...v0.1.18) (2021-03-01)


### Bug Fixes

* **cycle:** handle linked cycles no `dataCompleteness` ([fe96b1d](https://gitlab.com/hestia-earth/hestia-data-validation/commit/fe96b1d03e92e8d0e93e9babf6641b78ff5a1f9a))

### [0.1.17](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.16...v0.1.17) (2021-02-27)


### Bug Fixes

* **cycle:** remove unused validation of alStartDate ([d980e9f](https://gitlab.com/hestia-earth/hestia-data-validation/commit/d980e9f6badc42274aa8662a43299fd62221655c))

### [0.1.16](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.15...v0.1.16) (2021-02-26)


### Features

* **data completeness:** add warning all keys set to `false` ([333d2d3](https://gitlab.com/hestia-earth/hestia-data-validation/commit/333d2d31164382f7522a93f561b80aaf2f114938))
* **site:** validate coordinates with `siteType` ([909f840](https://gitlab.com/hestia-earth/hestia-data-validation/commit/909f840d49daac7c2ec4d2ae03e55506ad0b5015))


### Bug Fixes

* **cycle:** allow duplicated `products` ([bd64bf8](https://gitlab.com/hestia-earth/hestia-data-validation/commit/bd64bf896950d82295c7e3aabd11f0b65801738d)), closes [#33](https://gitlab.com/hestia-earth/hestia-data-validation/issues/33)

### [0.1.15](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.14...v0.1.15) (2021-02-12)


### Features

* return more than 1 error for lists ([e9d26e7](https://gitlab.com/hestia-earth/hestia-data-validation/commit/e9d26e76cc2b76ba9a63c763a2047f1107d9a102))
* **shared:** add warning for percent value ([8fd6eeb](https://gitlab.com/hestia-earth/hestia-data-validation/commit/8fd6eeb6a1a41b3584242fd66ea8657479a13f63)), closes [#18](https://gitlab.com/hestia-earth/hestia-data-validation/issues/18)
* **validators:** handle min/max as arrays ([885f399](https://gitlab.com/hestia-earth/hestia-data-validation/commit/885f399dbcb166b244759201b8927950272143fe))


### Bug Fixes

* **cycle:** validate duration only if start/end dates are in days ([6c3f248](https://gitlab.com/hestia-earth/hestia-data-validation/commit/6c3f248adffe66b9b9ecf4c6640d1941f044db63))
* **shared:** handle list of values for validation min/max ([6438e0a](https://gitlab.com/hestia-earth/hestia-data-validation/commit/6438e0a9568baa523fc01814db91ccdb08575f8e))

### [0.1.14](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.13...v0.1.14) (2021-02-05)


### Bug Fixes

* **measurement:** handle no texture value validate percent ([48b5df6](https://gitlab.com/hestia-earth/hestia-data-validation/commit/48b5df67fd351f2b471b6b582dd1b9b197137402)), closes [#31](https://gitlab.com/hestia-earth/hestia-data-validation/issues/31)

### [0.1.13](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.12...v0.1.13) (2021-01-29)


### Bug Fixes

* **shared:** handle value as string ([e865f6b](https://gitlab.com/hestia-earth/hestia-data-validation/commit/e865f6b05df2a03e91ae8e8b6164fab91e39ee68))

### [0.1.12](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.11...v0.1.12) (2021-01-15)


### Bug Fixes

* **utils:** handle sum float / str ([a3a1c5f](https://gitlab.com/hestia-earth/hestia-data-validation/commit/a3a1c5fed8de85f4c75ba4616416198e372700f8))

### [0.1.11](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.10...v0.1.11) (2021-01-14)

### [0.1.10](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.9...v0.1.10) (2021-01-13)


### Features

* validate value in percentage ([18249b7](https://gitlab.com/hestia-earth/hestia-data-validation/commit/18249b71b7a51c55b860677a5055b950c5fd36a4)), closes [#18](https://gitlab.com/hestia-earth/hestia-data-validation/issues/18)
* **cycle:** validate `products.economicValueShare` ([18d6e97](https://gitlab.com/hestia-earth/hestia-data-validation/commit/18d6e972d7a515c5813f5376774dd072c9ea4f96))
* **cycle:** validate sum `practices.value` for crop residue management ([b08a8a2](https://gitlab.com/hestia-earth/hestia-data-validation/commit/b08a8a2942aa81f37c6e3700771029338c52acfa)), closes [#25](https://gitlab.com/hestia-earth/hestia-data-validation/issues/25)

### [0.1.9](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.8...v0.1.9) (2021-01-12)


### Features

* **organisation:** validate coordinates in `region` ([a3f3d53](https://gitlab.com/hestia-earth/hestia-data-validation/commit/a3f3d53e2cd7ecd066368e7ece72bb7c6bf9a774))
* **site:** validate soil texture percentages ([355d60c](https://gitlab.com/hestia-earth/hestia-data-validation/commit/355d60c4f1e54238556be23e1865a97b63976f43)), closes [#28](https://gitlab.com/hestia-earth/hestia-data-validation/issues/28)


### Bug Fixes

* **site:** fix validate min/max soil texture ([0969c75](https://gitlab.com/hestia-earth/hestia-data-validation/commit/0969c7574f17b6df9e2169841bbfa4d43eebea4d))

### [0.1.8](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.7...v0.1.8) (2021-01-05)


### Bug Fixes

* **site:** validate soil texture only if all values are present ([c90342c](https://gitlab.com/hestia-earth/hestia-data-validation/commit/c90342c9b0a81f2ea8381a5ca73b27885f2abf01)), closes [#29](https://gitlab.com/hestia-earth/hestia-data-validation/issues/29)

### [0.1.7](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.6...v0.1.7) (2021-01-04)


### Features

* **organisation:** validate `country`, `area` and coordinates ([4abbcef](https://gitlab.com/hestia-earth/hestia-data-validation/commit/4abbcef26b48ac6c9300bbfe9bbd0f6e590b41ac))


### Bug Fixes

* **shared:** improve speed validate coordinates by region ([bdf4746](https://gitlab.com/hestia-earth/hestia-data-validation/commit/bdf47468b1eee8f172a96fd17a004e73a6d80b78))

### [0.1.6](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.5...v0.1.6) (2020-12-07)


### Features

* **site:** validate measurements value between min and max ([d7fc3f6](https://gitlab.com/hestia-earth/hestia-data-validation/commit/d7fc3f61ead723b71bd0819e4c51144be9ad9c6e)), closes [#7](https://gitlab.com/hestia-earth/hestia-data-validation/issues/7)


### Bug Fixes

* **validators:** allow "region-" terms to be used as countries ([0a71c3d](https://gitlab.com/hestia-earth/hestia-data-validation/commit/0a71c3d59f42248ac6c0074a66572a7d78c22942))

### [0.1.5](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.4...v0.1.5) (2020-11-29)


### Features

* remove checks on `termType` ([4ed008f](https://gitlab.com/hestia-earth/hestia-data-validation/commit/4ed008f5eb3e50934384402cad27106c5acd3bdd))
* **validators:** add check `termType` = `system` to practice ([7311adf](https://gitlab.com/hestia-earth/hestia-data-validation/commit/7311adf6aef3f469714503eb3a9a3e01e5abb249))


### Bug Fixes

* **cycle:** handle duplicates in emissions.inputs.[@id](https://gitlab.com/id) ([19b88cd](https://gitlab.com/hestia-earth/hestia-data-validation/commit/19b88cdd4596ac06254e1ec75949816f7f2fa8b5))

### [0.1.4](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.3...v0.1.4) (2020-11-26)


### Features

* **cycle:** validate min/max on emissions, inputs, products and practices ([3240f83](https://gitlab.com/hestia-earth/hestia-data-validation/commit/3240f83139af523ecb8aed12b3e366a11f93819a))
* **impact assessment:** add validators country, region and impacts ([12f29dc](https://gitlab.com/hestia-earth/hestia-data-validation/commit/12f29dc7973755b1db1ed40462bfd27acdf4f599))
* **impact assessment:** validate duplicates in impacts/emissionsResourceUse ([c2d36dc](https://gitlab.com/hestia-earth/hestia-data-validation/commit/c2d36dcf3209e905a334569eb9b11296c720755a))
* **site:** validate min/max on measurements ([2d4e8b4](https://gitlab.com/hestia-earth/hestia-data-validation/commit/2d4e8b44aab5528cf5c27f618bd43d0001789ba5))
* **term:** handle termType check on lists ([adbe8ff](https://gitlab.com/hestia-earth/hestia-data-validation/commit/adbe8ff5b503d378b63a818776431b91e0acdcb0))


### Bug Fixes

* **property-to-termType.csv:** update termType matching ([524d292](https://gitlab.com/hestia-earth/hestia-data-validation/commit/524d2927d858082959220e005ee29d15e456b438))

### [0.1.3](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.2...v0.1.3) (2020-11-13)


### Bug Fixes

* **package:** add missing file in manifest ([9c72929](https://gitlab.com/hestia-earth/hestia-data-validation/commit/9c7292960ec616a23809da7b406d12971da1816b))

### [0.1.2](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.1...v0.1.2) (2020-11-13)


### Bug Fixes

* **cycle:** check duplicated emissions including input.[@id](https://gitlab.com/id) ([bd508d0](https://gitlab.com/hestia-earth/hestia-data-validation/commit/bd508d0381f87e62570fea89869539162ac0b102))

### [0.1.1](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.1.0...v0.1.1) (2020-11-13)


### Features

* **validators:** add more termType validations ([f76fea5](https://gitlab.com/hestia-earth/hestia-data-validation/commit/f76fea50b8ece11ee56c6be565ad5f8e3c79c8d0))

## [0.1.0](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.14...v0.1.0) (2020-11-12)


### Features

* **cycle:** validate altStartDate not with startDate ([b08acab](https://gitlab.com/hestia-earth/hestia-data-validation/commit/b08acab90268ae779404b582809545062322e9cd))
* **cycle:** validate must have altStartDateDefinition with altStartDate ([d4a92df](https://gitlab.com/hestia-earth/hestia-data-validation/commit/d4a92df4781a76de2411e4fe1f6b56f6e3c01f0e))
* **site:** add validator on country ([66f98db](https://gitlab.com/hestia-earth/hestia-data-validation/commit/66f98db992777c4c96d5cef8747ae32745e1479d)), closes [#16](https://gitlab.com/hestia-earth/hestia-data-validation/issues/16)


### Bug Fixes

* **lookups:** update termTypes from schema 0.2.0 ([a9be63a](https://gitlab.com/hestia-earth/hestia-data-validation/commit/a9be63a6703543c7e2669ecaec49f6f5fb7d5565))

### [0.0.14](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.13...v0.0.14) (2020-11-09)


### Features

* this is a drop-in replacement of version 0.0.13 ([c59d7c3](https://gitlab.com/hestia-earth/hestia-data-validation/commit/c59d7c37d69caeecc6d050e7cad3af09a3d69a56))

### [0.0.13](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.12...v0.0.13) (2020-11-09)


### Bug Fixes

* add missing files in package ([92e2c42](https://gitlab.com/hestia-earth/hestia-data-validation/commit/92e2c4281e40da92b617f6b2ac13f450a8963c8a))

### [0.0.12](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.11...v0.0.12) (2020-11-09)


### Features

* **validators:** add missing country termType checks ([bba0475](https://gitlab.com/hestia-earth/hestia-data-validation/commit/bba04759088778446b3bfe033e42baf0ac580e58))
* **validators:** validate node children termType ([9de8d14](https://gitlab.com/hestia-earth/hestia-data-validation/commit/9de8d14c88124377fa5be7aaa2d586f0afc20bb2)), closes [#15](https://gitlab.com/hestia-earth/hestia-data-validation/issues/15)


### Bug Fixes

* **validators:** add missing Emission.input termType check ([e97058d](https://gitlab.com/hestia-earth/hestia-data-validation/commit/e97058db5fb9e3d56ffe9dd6bd6107814ac5fcf3)), closes [#15](https://gitlab.com/hestia-earth/hestia-data-validation/issues/15)
* **validators:** handle dataPath on subchildren error ([8306a41](https://gitlab.com/hestia-earth/hestia-data-validation/commit/8306a41a46a89f4efd291c7ece7b0129d6afaafd))
* **validators:** remove destination termType from Inventory.product ([ea0c58d](https://gitlab.com/hestia-earth/hestia-data-validation/commit/ea0c58d3877492ed2bc83b3a09c6343421f3d413)), closes [#15](https://gitlab.com/hestia-earth/hestia-data-validation/issues/15)

### [0.0.11](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.10...v0.0.11) (2020-11-06)


### Features

* **validators site:** get faster results check coordinates in country ([82b009a](https://gitlab.com/hestia-earth/hestia-data-validation/commit/82b009a2886e73371c4fa94d45d16363f6b12e56))

### [0.0.10](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.9...v0.0.10) (2020-11-02)


### Bug Fixes

* **site:** update error message on region not in country ([a6c68f3](https://gitlab.com/hestia-earth/hestia-data-validation/commit/a6c68f3d3b2805880f3c2b256b0614f1b0046c9c))

### [0.0.9](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.8...v0.0.9) (2020-09-17)


### Features

* **site:** skip validate coordinates for non-inland types ([15484b7](https://gitlab.com/hestia-earth/hestia-data-validation/commit/15484b7066dbb6af4604181dd5b28c731d521f96)), closes [#13](https://gitlab.com/hestia-earth/hestia-data-validation/issues/13)

### [0.0.8](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.7...v0.0.8) (2020-09-17)


### Bug Fixes

* **gadm:** fix parsing of level from id ([ed902e5](https://gitlab.com/hestia-earth/hestia-data-validation/commit/ed902e5b27579b82604d301a456395e67308b5a4))

### [0.0.7](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.6...v0.0.7) (2020-09-16)


### Features

* **site:** test region is within country ([6291ed1](https://gitlab.com/hestia-earth/hestia-data-validation/commit/6291ed1e0fdecc47cc5da92c88db88340c2219ed))
* **site:** validate coordinates in country/region ([2eb2977](https://gitlab.com/hestia-earth/hestia-data-validation/commit/2eb2977ab67979822c879021c4a65ead45a81fde)), closes [#4](https://gitlab.com/hestia-earth/hestia-data-validation/issues/4)
* **validation:** run each node in parallel ([e007eed](https://gitlab.com/hestia-earth/hestia-data-validation/commit/e007eedc25b44c7759670a6536db39afff333a96))

### [0.0.6](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.5...v0.0.6) (2020-09-15)


### Bug Fixes

* allow equal dates when day is not specified ([81fe065](https://gitlab.com/hestia-earth/hestia-data-validation/commit/81fe065006ebd6da09f9030f06baae1e390121f5))

### [0.0.5](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.4...v0.0.5) (2020-09-05)


### Features

* **cycle:** check for duplicates in emissions, inputs and products ([68ba87a](https://gitlab.com/hestia-earth/hestia-data-validation/commit/68ba87a9e6b8536a9886bd5f72d30538046c525d)), closes [#2](https://gitlab.com/hestia-earth/hestia-data-validation/issues/2)
* **cycle:** validate children relDays and value length ([d581a60](https://gitlab.com/hestia-earth/hestia-data-validation/commit/d581a605cfa54d5ac67e4697e97d5ef1e5940c3f)), closes [#12](https://gitlab.com/hestia-earth/hestia-data-validation/issues/12)
* **site:** check area from boundary ([bf0dae4](https://gitlab.com/hestia-earth/hestia-data-validation/commit/bf0dae4c070ad7f325a0909cc8ada07f90c43375)), closes [#11](https://gitlab.com/hestia-earth/hestia-data-validation/issues/11)
* **site:** check for duplicates in measurements ([b4ca753](https://gitlab.com/hestia-earth/hestia-data-validation/commit/b4ca7531c3ebb31c14a4162fb595252e23f9c54d))

### [0.0.4](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.3...v0.0.4) (2020-09-04)


### Features

* **cycle:** valid emissions dates ([caa3b9f](https://gitlab.com/hestia-earth/hestia-data-validation/commit/caa3b9f79d22a67ffcaa3c97c1472a0de58a2d7a)), closes [#5](https://gitlab.com/hestia-earth/hestia-data-validation/issues/5)
* **cycle:** validate cycleDuration ([01952c0](https://gitlab.com/hestia-earth/hestia-data-validation/commit/01952c0c2ef4c26226ed278215874eb75da5c218)), closes [#9](https://gitlab.com/hestia-earth/hestia-data-validation/issues/9)
* **cycle:** validate endDate > startDate ([9538d61](https://gitlab.com/hestia-earth/hestia-data-validation/commit/9538d6111ca71276ab4758a1106785a0ca15e727))
* **cycle:** validate functionalUnitMeasure ([ed180f5](https://gitlab.com/hestia-earth/hestia-data-validation/commit/ed180f5019115179cbf3735964a611b0dbf5c377)), closes [#10](https://gitlab.com/hestia-earth/hestia-data-validation/issues/10)
* **cycle:** validate practices endDate > startDate ([df8fe14](https://gitlab.com/hestia-earth/hestia-data-validation/commit/df8fe14dc3ec979690a2bef7dd8706798169cfb1)), closes [#5](https://gitlab.com/hestia-earth/hestia-data-validation/issues/5)
* **organisation:** validate endDate > startDate ([286e21e](https://gitlab.com/hestia-earth/hestia-data-validation/commit/286e21eb83bf8e7c7cd6b66e2674afaef6bc23d2)), closes [#5](https://gitlab.com/hestia-earth/hestia-data-validation/issues/5)
* **site:** check lifespan for infrastructure ([0fa6e99](https://gitlab.com/hestia-earth/hestia-data-validation/commit/0fa6e99549642fbb3c4e651602c5be5edee1eecd)), closes [#8](https://gitlab.com/hestia-earth/hestia-data-validation/issues/8)
* **site:** show calculated lifespan in error message ([d5097f3](https://gitlab.com/hestia-earth/hestia-data-validation/commit/d5097f332ca22cd5dc34e8772a9e63b4de22dad9))
* **site:** valid measurements dates ([dc45c07](https://gitlab.com/hestia-earth/hestia-data-validation/commit/dc45c0709cb56ac929f34e703d4eb6c5ff8fcde3)), closes [#5](https://gitlab.com/hestia-earth/hestia-data-validation/issues/5)
* **site:** validate depthUpper > depthLower ([8dc5524](https://gitlab.com/hestia-earth/hestia-data-validation/commit/8dc5524064f5e61298949b85a2175ea9f821a557)), closes [#6](https://gitlab.com/hestia-earth/hestia-data-validation/issues/6)
* **site:** validate endDate > startDate ([0681986](https://gitlab.com/hestia-earth/hestia-data-validation/commit/06819864ce52edc8f031c92f11ed5b00cd1b7dda))
* **site:** validate infrastructure endDate > startDate ([a201535](https://gitlab.com/hestia-earth/hestia-data-validation/commit/a201535f841b821356999962905f440dd5f414a5)), closes [#5](https://gitlab.com/hestia-earth/hestia-data-validation/issues/5)


### Bug Fixes

* **site:** fix check depthUpper < depthLower ([3d90848](https://gitlab.com/hestia-earth/hestia-data-validation/commit/3d90848f096f409d901ebfad5bab6d9a1eea8492)), closes [#6](https://gitlab.com/hestia-earth/hestia-data-validation/issues/6)

### [0.0.3](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.2...v0.0.3) (2020-08-14)


### Features

* **validators:** handle recursive validation ([7e50335](https://gitlab.com/hestia-earth/hestia-data-validation/commit/7e5033511ed70d28def4bcae0b0ad4a9dd085292))


### Bug Fixes

* **site:** handle no 'measurements' key ([466ff48](https://gitlab.com/hestia-earth/hestia-data-validation/commit/466ff488f73d193f35bd878a8966de51aa7e4c91))
* **validators:** handle both type and [@type](https://gitlab.com/type) ([cc6601e](https://gitlab.com/hestia-earth/hestia-data-validation/commit/cc6601e3cd096ef01c9adc7d75def28f856fc694))

### [0.0.2](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.1...v0.0.2) (2020-08-13)


### Bug Fixes

* **site:** handle type instead of [@type](https://gitlab.com/type) ([cb00db5](https://gitlab.com/hestia-earth/hestia-data-validation/commit/cb00db509aabb62e87eb9000285febd7e64ed27c))

### [0.0.1](https://gitlab.com/hestia-earth/hestia-data-validation/compare/v0.0.0...v0.0.1) (2020-08-11)


### Features

* **site:** add site validation on measurements ([ad83051](https://gitlab.com/hestia-earth/hestia-data-validation/commit/ad83051343367703df7852009c90b8f98bf8a517))
* **site:** set message level ([e6137ab](https://gitlab.com/hestia-earth/hestia-data-validation/commit/e6137abd69fd2b5c6fd288d9f8df897e005c6f27))
* **validators:** add dummy site validation ([4a9d0f4](https://gitlab.com/hestia-earth/hestia-data-validation/commit/4a9d0f493ee4eece69e5b33d44110bf58a3405b6))


### Bug Fixes

* **site:** handle case multiple soilType values for same term ([45e2e55](https://gitlab.com/hestia-earth/hestia-data-validation/commit/45e2e558d332586ec67ddb718644de8b3854f2ae))
* **site:** handle measurements same depth fields ([f7556de](https://gitlab.com/hestia-earth/hestia-data-validation/commit/f7556de5f4250357e3f91e8b682bb316ffdc2fe3))
