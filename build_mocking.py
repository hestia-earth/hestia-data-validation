import sys
import json
from hestia_earth.validation.terms import RESULTS_PATH, get_all_terms


def main(args: list):
    filepath = args[0] if len(args) > 0 else RESULTS_PATH
    data = get_all_terms()
    with open(filepath, 'w') as f:
        f.write(json.dumps(data, indent=2, ensure_ascii=False))


if __name__ == "__main__":
    main(sys.argv[1:])
