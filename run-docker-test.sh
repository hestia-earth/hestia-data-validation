#!/bin/sh
docker build -t hestia-data-validation:test -f tests/Dockerfile .

docker run --rm \
  --env-file .env \
  -v ${PWD}:/app \
  hestia-data-validation:test "$@"
