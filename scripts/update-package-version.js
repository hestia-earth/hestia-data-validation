const { readFileSync, writeFileSync } = require('fs');
const { resolve, join } = require('path');

const ROOT = resolve(join(__dirname, '../'));
const encoding = 'UTF-8';
const version = require(join(ROOT, 'package.json')).version;

const VERSION_PATH = resolve(join(ROOT, 'hestia_earth', 'validation', 'version.py'));
let content = readFileSync(VERSION_PATH, encoding);
content = content.replace(/VERSION\s=\s\'[\d\-a-z\.]+\'/, `VERSION = '${version}'`);
writeFileSync(VERSION_PATH, content, encoding);

[
  resolve(join(ROOT, 'requirements-ci.txt'))
].map(path => {
  let content = readFileSync(path, encoding);
  content = content.replace(/hestia_earth.validation>=[\d\-a-z\.]+/, `hestia_earth.validation>=${version}`);
  writeFileSync(path, content, encoding);
});
