# New Validation

## Validation

<!-- Please describe what the validation does -->

### Validation Conditions

<!--

Please describe the conditions when the validation will return the error/warning message.

Example:
- If the Cycle `functionalUnit` is equal to `1 ha`
- If the primary Product `units` is equal to `ha`
- If the product `value` is equal to `0`
- Then show error

-->

### Validation Level

<!-- Please select the validation level by entering an "x" instead of the space in the brackets -->

- [ ] Warning: this might be an error, but we will still allow the upload to be validated.
- [ ] Error: this is an error and must be fixed to validate the upload.

## Example

<!-- 

Please provide an example of data to validate and the validation message below.

Note: you can write the example in a Excel (export to CSV) or directly a CSV file, and use the [Convert Files tool](https://www.hestia.earth/files/tools) on the platform to convert to JSON.

-->

### Input Data

<!--
Example:

```
{
  "@type": "Cycle",
  "functionalUnit: "1ha",
  "products": [
    {
      "@type": "Product",
      "term": {
        "units": "ha"
      },
      "value": [
        0
      ]
    }
  ]
}
```
-->


### Output error/warning message

<!--
Use the following structure:
- Describe error.
- Say why important to fix (especially for warnings where they are optional).
- Describe how to fix (if not obvious).

```
The value for the a Product in ha must be above `0` or we can not calculate an impact.
```
-->

## Additional Notes

<!-- Add further notes if necessary -->

/label ~"Type::feature"
/label ~"Priority::LOW"
/label ~"Tracking::Triage"
