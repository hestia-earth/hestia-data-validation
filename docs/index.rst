Welcome to HESTIA Data Validation's documentation!
========================================

This package contains the Data Validation module developed by HESTIA.

Installation
------------

Install from `PyPI <https://pypi.python.org/pypi>`_ using `pip <http://www.pip-installer.org/en/latest/>`_, a
package manager for Python.

.. code-block:: bash

    pip install hestia_earth.validation


Requirements
============

- `hestia_earth.schema <https://pypi.org/project/hestia-earth.schema/>`_
- `requests <https://pypi.org/project/requests/>`_
- `python-dateutil <https://pypi.org/project/python-dateutil/>`_
- `area <https://pypi.org/project/area/>`_

Contents
--------

.. autosummary::
   :toctree: _autosummary
   :caption: API Reference
   :template: custom-module-template.rst
   :recursive:

   hestia_earth.validation


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
