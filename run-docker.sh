#!/bin/sh

docker build --progress=plain -t hestia-data-validation:latest .

docker run --rm \
  --name hestia-data-validation \
  -v ${PWD}:/app \
  -v ${PWD}/samples:/app/samples \
  hestia-data-validation:latest python run.py "$@"
